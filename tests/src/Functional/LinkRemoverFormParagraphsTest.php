<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\entity_usage\Traits\EntityUsageLastEntityQueryTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Tests Link Remover form with a large number of paragraphs and fields.
 *
 * @group entity_usage_updater
 */
class LinkRemoverFormParagraphsTest extends BrowserTestBase {
  use EntityUsageLastEntityQueryTrait;
  use ParagraphsTestBaseTrait;

  /**
   * The number of paragraphs to create.
   */
  private const NUMBER_OF_PARAGRAPHS = 9;

  /**
   * The number of text field on page nodes to create.
   */
  private const NUMBER_OF_FIELDS = 6;

  /**
   * The number of nodes to add links to.
   */
  private const NUMBER_OF_NODES = 3;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'text',
    'link',
    'path',
    'path_alias',
    'paragraphs',
    'entity_usage_updater',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->addParagraphedContentType('page');
    $this->addParagraphsType('test_paragraph');

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_paragraph_text',
      'entity_type' => 'paragraph',
      'type' => 'text_long',
      'cardinality' => 1,
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_paragraph',
      'label' => 'Paragraph text field',
    ])->save();

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('paragraph', 'test_paragraph');
    $page_display = \Drupal::service('entity_display.repository')->getViewDisplay('paragraph', 'test_paragraph');
    $form_display->setComponent('field_paragraph_text', ['type' => 'text_textarea']);
    $page_display->setComponent('field_paragraph_text', ['type' => 'text_default']);
    $form_display->save();
    $page_display->save();

    // Add a few text fields to our test content type.
    for ($i = 1; $i <= self::NUMBER_OF_FIELDS; $i++) {
      $field_storage = FieldStorageConfig::create([
        'field_name' => 'field_text_' . $i,
        'entity_type' => 'node',
        'type' => 'text_long',
        'cardinality' => 1,
      ]);
      $field_storage->save();

      FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => 'page',
        'label' => 'Text field ' . $i,
      ])->save();
    }

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('node', 'page', 'default');
    $page_display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page', 'default');
    for ($i = 1; $i <= self::NUMBER_OF_FIELDS; $i++) {
      $form_display->setComponent('field_text_' . $i, ['type' => 'text_textarea']);
      $page_display->setComponent('field_text_' . $i, ['type' => 'text_default']);
    }
    $form_display->save();
    $page_display->save();

    // Set up the filter formats used by this test.
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a href> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();
    user_role_grant_permissions('authenticated', [$basic_html_format->getPermissionName(), 'access content']);

    $current_request = \Drupal::request();
    $config = \Drupal::configFactory()->getEditable('entity_usage.settings');
    $config->set('site_domains', [$current_request->getHttpHost() . $current_request->getBasePath()]);
    $config->save();
    $this->config('entity_usage.settings')->set('local_task_enabled_entity_types', ['node'])->save();
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests the link remover with lots of fields and paragraphs.
   */
  public function testLargeNumberOfParagraphsAndFields(): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $account = $this->drupalCreateUser(['create page content', 'update referenced entities', 'create url aliases']);
    $this->drupalLogin($account);

    // Create node 1.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 1');
    $page->fillField('path[0][alias]', '/alias_node_1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 1 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $this->drupalGetNodeByTitle('Node 1');

    // Create node 2.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 2');
    $page->fillField('path[0][alias]', '/alias_node_2');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 2 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node2 = $this->drupalGetNodeByTitle('Node 2');

    // Create node 3.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 3');
    $page->fillField('path[0][alias]', '/alias_node_3');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 3 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node3 = $this->drupalGetNodeByTitle('Node 3');

    // Need to rebuild to allow aliases to be  generated by Url class.
    $this->rebuildAll();
    $ignored_link = $node3->toLink('Not removed', options:  ['absolute' => TRUE])->toString();
    $paragraph_text = "<p>" . $node1->toLink('Non-alias link', options:  ['path_processing' => FALSE, 'absolute' => TRUE])->toString() . "</p><p>" . $node1->toLink('Aliased link', options:  ['absolute' => TRUE])->toString() . "</p><p>" . $node1->toLink('Another non-alias link', options:  ['path_processing' => FALSE, 'absolute' => TRUE])->toString() . "</p>";
    $text_field_text = "<p>" . $ignored_link . "</p><p>" . $node2->toLink('Non-alias link2', options:  ['path_processing' => FALSE, 'absolute' => TRUE])->toString() . "</p><p>" . $node2->toLink('Aliased link2', options:  ['absolute' => TRUE])->toString() . "</p><p>" . $ignored_link . "</p><p>" . $node2->toLink('Another non-alias link2', options:  ['path_processing' => FALSE, 'absolute' => TRUE])->toString() . "</p><p>" . $ignored_link . "</p>";

    $nodes_to_check = [];
    for ($j = 1; $j <= self::NUMBER_OF_NODES; $j++) {
      $paragraphs = [];
      for ($i = 1; $i <= self::NUMBER_OF_PARAGRAPHS; $i++) {
        $paragraphs[$i] = Paragraph::create([
          'type' => 'test_paragraph',
          'field_paragraph_text' => [
            ['value' => $paragraph_text, 'format' => 'basic_html'],
          ],
        ]);
        $paragraphs[$i]->save();
      }

      // Create a node with paragraphs.
      $node = Node::create([
        'title' => 'Node with links: ' . $j,
        'type' => 'page',
        'field_paragraphs' => $paragraphs,
      ]);
      for ($i = 1; $i <= self::NUMBER_OF_FIELDS; $i++) {
        $field_name = 'field_text_' . $i;
        // @phpstan-ignore-next-line assign.propertyType
        $node->$field_name = ['value' => $text_field_text, 'format' => 'basic_html'];
      }
      $node->save();

      $nodes_to_check[] = $node->id();

      $this->drupalGet('node/' . $node->id());
      $this->assertSession()->linkByHrefExists($node1->toUrl()->toString());
      $this->assertSession()->linkByHrefExists($node1->toUrl(options: ['path_processing' => FALSE])->toString());
      $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());
      $this->assertSession()->linkByHrefExists($node2->toUrl(options: ['path_processing' => FALSE])->toString());
      // Count non-aliased links.
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node1->toUrl(options: ['path_processing' => FALSE])->toString()]));
      $this->assertCount(self::NUMBER_OF_PARAGRAPHS * 2, $links);
      // Count aliased links.
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node1->toUrl()->toString()]));
      $this->assertCount(self::NUMBER_OF_PARAGRAPHS, $links);

      // Count non-aliased links.
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node2->toUrl(options: ['path_processing' => FALSE])->toString()]));
      $this->assertCount(self::NUMBER_OF_FIELDS * 2, $links);
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node2->toUrl()->toString()]));
      $this->assertCount(self::NUMBER_OF_FIELDS, $links);

      // Count ignored links.
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node3->toUrl()->toString()]));
      $this->assertCount(self::NUMBER_OF_FIELDS * 3, $links);
    }

    $this->drupalGet('admin/config/content/link-remover');
    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
      $node2->toUrl()->setAbsolute()->toString(),
    ]));

    $this->assertSession()->buttonExists('Remove')->press();
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();

    // Check to see if the links have been removed.
    foreach ($nodes_to_check as $node_id) {
      $this->drupalGet('node/' . $node_id);
      $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());
      $this->assertSession()->linkByHrefNotExists($node1->toUrl(options: ['path_processing' => FALSE])->toString());
      $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());
      $this->assertSession()->linkByHrefNotExists($node2->toUrl(options: ['path_processing' => FALSE])->toString());
      // Count ignored links.
      $links = $page->findAll('xpath', $assert_session->buildXPathQuery('//a[contains(@href, :href)]', [':href' => $node3->toUrl()->toString()]));
      $this->assertCount(self::NUMBER_OF_FIELDS * 3, $links);
    }
  }

}
