<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\content_moderation\Functional\ModerationStateTestBase;
use Drupal\Tests\entity_usage\Traits\EntityUsageLastEntityQueryTrait;
use Drupal\user\Entity\Role;

/**
 * Tests the Link Remover form with content moderation.
 *
 * @group entity_usage_updater
 */
class ContentModerationLinkRemoverFormTest extends ModerationStateTestBase {

  protected $defaultTheme = 'stark';

  use EntityUsageLastEntityQueryTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'text',
    'path',
    'entity_usage_updater',
    'content_moderation',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->permissions[] = 'update referenced entities';
    $this->permissions[] = 'administer site configuration';
    parent::setUp();

    $this->drupalLogin($this->adminUser);
    $this->createContentTypeFromUi('Page', 'page', TRUE);
    $this->grantUserPermissionToCreateContentOfType($this->adminUser, 'page');

    // Set up the filter formats used by this test.
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a href> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();
    user_role_grant_permissions('authenticated', [$basic_html_format->getPermissionName(), 'access content']);

    $current_request = \Drupal::request();
    $config = \Drupal::configFactory()->getEditable('entity_usage.settings');
    $config->set('site_domains', [$current_request->getHttpHost() . $current_request->getBasePath()]);
    $config->save();
    $this->config('entity_usage.settings')
      ->set('local_task_enabled_entity_types', ['node'])
      ->save();
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests the link remover.
   *
   * @testWith [true]
   *           [false]
   *
   * @covers \Drupal\entity_usage_updater\Form\LinkRemoverForm
   */
  public function testLinkRemover(bool $unpublish): void {
    [$node1, $node2, $node3] = $this->createContent();

    $this->drupalGet('admin/config/content/link-remover');
    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
      $node2->toUrl()->setAbsolute()->toString(),
    ]));
    $this->assertSession()->fieldExists('unpublish')->setValue($unpublish);
    $this->assertSession()->buttonExists('Remove')->press();

    if ($unpublish) {
      // Ensure the content moderation options are as expected.
      $this->assertSession()->fieldExists('table[node|1][moderation]');
      $this->assertSession()->optionNotExists('table[node|1][moderation]', 'published');
      $this->assertSession()->optionExists('table[node|1][moderation]', 'draft');
      $this->assertSession()->fieldValueEquals('table[node|1][moderation]', 'archived');

      // There are no valid options to change the entity to in order to
      // unpublish it.
      $this->assertSession()->fieldNotExists('table[node|2][moderation]');
    }

    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[1]', (string) $node1->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[2]', (string) $node1->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[3]', '1');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[4]', 'Published');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[1]', (string) $node2->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[2]', (string) $node2->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[3]', '2');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[4]', 'Draft');

    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    $this->assertSession()->pageTextContains('Updated one entity for references to Node 1.');
    $this->assertSession()->pageTextContains('Updated 2 entities for references to Node 2.');
    if ($unpublish) {
      $this->assertSession()->pageTextContains('Updated moderation state for Node 1 to Archived.');
      $this->assertSession()->pageTextNotContains('Updated moderation state for Node 2');
    }

    // Check to see if the links have been removed.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());
    $this->drupalGet('node/4');
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());

    // Check to see if the link node 2 has been removed but the link to node 3
    // is still there.
    $this->drupalGet('node/5');
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());
    $this->assertSession()->linkByHrefExists($node3->toUrl()->toString());

    if ($unpublish) {
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      $node_storage->resetCache();
      $node1 = $node_storage->load($node1->id());
      $node2 = $node_storage->load($node2->id());
      $this->assertSame('archived', $node1->get('moderation_state')->value, 'Node 1 is archived.');
      $this->assertSame('draft', $node2->get('moderation_state')->value, 'Node 2 is a draft.');
    }
  }

  /**
   * Tests the link remover with content moderation and a default state set.
   *
   * @covers \Drupal\entity_usage_updater\Form\LinkRemoverForm
   */
  public function testLinkRemoverWithContentModerationDefault(): void {
    $this->drupalGet('admin/config/content/entity-usage-updater');
    $this->assertCount(1, $this->assertSession()
      ->fieldExists('workflow[editorial]')
      ->findAll('css', 'option'));

    $this->drupalGet('admin/config/workflow/workflows/manage/editorial/add_state');
    $this->assertSession()->fieldExists('label')->setValue('Links removed');
    $this->assertSession()->fieldExists('id')->setValue('links_removed');
    $this->assertSession()
      ->fieldExists('type_settings[default_revision]')
      ->check();
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Created Links removed state.');

    $this->getSession()->getPage()->clickLink('Add a new transition');
    $this->assertSession()->fieldExists('label')->setValue('Links remover');
    $this->assertSession()->fieldExists('id')->setValue('links_remover');
    foreach ($this->assertSession()->elementExists('css', '#edit-from')->findAll('css', 'input[type=\'checkbox\']') as $checkbox) {
      $checkbox->check();
    }
    $this->assertSession()->fieldExists('to')->selectOption('links_removed');
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()
      ->pageTextContains('Created Links remover transition.');

    $this->drupalGet('admin/config/content/entity-usage-updater');
    $this->assertCount(2, $this->assertSession()
      ->fieldExists('workflow[editorial]')
      ->findAll('css', 'option'));
    $this->assertSession()
      ->fieldExists('workflow[editorial]')
      ->selectOption('links_removed');
    $this->assertSession()->buttonExists('Save')->press();

    // Allow the user to use the new transition.
    $role_ids = $this->adminUser->getRoles(TRUE);
    $role_id = reset($role_ids);
    $role = Role::load($role_id);
    $role->grantPermission('use editorial transition links_remover')->save();

    [$node1, $node2] = $this->createContent();

    $this->drupalGet('admin/config/content/link-remover');
    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
      $node2->toUrl()->setAbsolute()->toString(),
    ]));
    $this->assertSession()->fieldExists('unpublish')->check();
    $this->assertSession()->buttonExists('Remove')->press();

    // Ensure the content moderation options are as expected.
    $this->assertSession()->optionNotExists('table[node|1][moderation]', 'published');
    $this->assertSession()->optionExists('table[node|1][moderation]', 'draft');
    $this->assertSession()->fieldValueEquals('table[node|1][moderation]', 'links_removed');

    $this->assertSession()->optionNotExists('table[node|2][moderation]', 'draft');
    $this->assertSession()->optionNotExists('table[node|2][moderation]', 'published');
    $this->assertSession()->optionNotExists('table[node|2][moderation]', 'archived');
    $this->assertSession()->fieldValueEquals('table[node|1][moderation]', 'links_removed');

    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[1]', (string) $node1->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[2]', (string) $node1->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[3]', '1');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[4]', 'Published');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[1]', (string) $node2->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[2]', (string) $node2->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[3]', '2');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[4]', 'Draft');

    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    $this->assertSession()->pageTextContains('Updated one entity for references to Node 1.');
    $this->assertSession()->pageTextContains('Updated 2 entities for references to Node 2.');

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node_storage->resetCache();
    $node1 = $node_storage->load($node1->id());
    $node2 = $node_storage->load($node2->id());
    $this->assertSame('links_removed', $node1->get('moderation_state')->value, 'Node 1 is in link_removed state.');
    $this->assertSame('links_removed', $node2->get('moderation_state')->value, 'Node 2 is in link_removed state.');
  }

  /**
   * Tests entity_usage_updater_workflow_presave().
   */
  public function testWorkflowStateRemoval(): void {
    $this->drupalGet('admin/config/content/entity-usage-updater');
    $this->assertCount(1, $this->assertSession()->fieldExists('workflow[editorial]')->findAll('css', 'option'));

    $this->drupalGet('admin/config/workflow/workflows/manage/editorial/add_state');
    $this->assertSession()->fieldExists('label')->setValue('Links removed');
    $this->assertSession()->fieldExists('id')->setValue('links_removed');
    $this->assertSession()->fieldExists('type_settings[default_revision]')->check();
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Created Links removed state.');

    $this->getSession()->getPage()->clickLink('Add a new transition');
    $this->assertSession()->fieldExists('label')->setValue('Links remover');
    $this->assertSession()->fieldExists('id')->setValue('links_remover');
    foreach ($this->assertSession()->elementExists('css', '#edit-from')->findAll('css', 'input[type=\'checkbox\']') as $checkbox) {
      $checkbox->check();
    }
    $this->assertSession()->fieldExists('to')->selectOption('links_removed');
    $this->assertSession()->buttonExists('Save')->press();
    $this->assertSession()->pageTextContains('Created Links remover transition.');

    $this->drupalGet('admin/config/content/entity-usage-updater');
    $this->assertCount(2, $this->assertSession()->fieldExists('workflow[editorial]')->findAll('css', 'option'));
    $this->assertSession()->fieldExists('workflow[editorial]')->selectOption('links_removed');
    $this->assertSession()->buttonExists('Save')->press();

    $workflow_storage = \Drupal::entityTypeManager()->getStorage('workflow');
    /** @var \Drupal\workflows\WorkflowInterface $workflow */
    $workflow = $workflow_storage->load('editorial');
    $this->assertSame('links_removed', $workflow->getThirdPartySetting('entity_usage_updater', 'default_unpublished_state'));
    $this->drupalGet('admin/config/workflow/workflows/manage/editorial/state/links_removed/delete');
    $this->assertSession()->buttonExists('Delete')->press();
    $workflow_storage->resetCache();
    $workflow = $workflow_storage->load('editorial');
    $this->assertNull($workflow->getThirdPartySetting('entity_usage_updater', 'default_unpublished_state'));
  }

  /**
   * Creates content for testing.
   *
   * @return \Drupal\node\NodeInterface[]
   *   The created content in an array.
   */
  private function createContent(): array {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // Create node 1.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 1');
    $page->selectFieldOption('moderation_state[0][state]', 'published');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 1 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $this->drupalGetNodeByTitle('Node 1');

    // Create node 2.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 2');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 2 has been created.');
    $node2 = $this->drupalGetNodeByTitle('Node 2');

    // Create node 3.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 3');
    $page->fillField('body[0][value]', (string) $node1->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->selectFieldOption('moderation_state[0][state]', 'published');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 3 has been created.');
    $node3 = $this->drupalGetNodeByTitle('Node 3');
    $this->assertSession()->linkByHrefExists($node1->toUrl()->toString());

    // Create node 4.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 4');
    $page->fillField('body[0][value]', (string) $node2->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->selectFieldOption('moderation_state[0][state]', 'published');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 4 has been created.');
    $node4 = $this->drupalGetNodeByTitle('Node 4');
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());

    // Create node 5.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 5');
    $page->fillField('body[0][value]', $node3->toLink("Link", options: ['absolute' => TRUE])->toString() . $node2->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 5 has been created.');
    $node5 = $this->drupalGetNodeByTitle('Node 5');
    $this->assertSession()->linkByHrefExists($node3->toUrl()->toString());
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());

    return [$node1, $node2, $node3, $node4, $node5];
  }

}
