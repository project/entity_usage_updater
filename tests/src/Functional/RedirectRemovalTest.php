<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the link remover with redirects.
 *
 * @group entity_usage_updater
 */
class RedirectRemovalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_usage',
    'entity_usage_updater',
    'filter',
    'node',
    'path',
    'path_alias',
    'redirect',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up the filter formats used by this test.
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a href> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();

    $current_request = \Drupal::request();
    $this->config('entity_usage.settings')
      ->set('local_task_enabled_entity_types', ['node'])
      ->set('track_enabled_source_entity_types', ['node', 'redirect'])
      ->set('track_enabled_target_entity_types', ['node', 'redirect'])
      ->set('track_enabled_plugins', ['html_link', 'link'])
      ->set('site_domains', [$current_request->getHttpHost() . $current_request->getBasePath()])
      ->save();

    /** @var \Drupal\Core\Routing\RouteBuilderInterface $routerBuilder */
    $routerBuilder = \Drupal::service('router.builder');
    $routerBuilder->rebuild();

    $this->drupalLogin($this->drupalCreateUser(admin: TRUE));

    $this->drupalCreateContentType(['type' => 'page']);
  }

  /**
   * Tests redirects usages are also removed by the link remover.
   */
  public function testTrackingRedirectAndAliasChange(): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // Create node 1.
    $this->drupalGet('/node/add/page');
    $page->fillField('title[0][value]', 'One');
    $page->fillField('path[0][alias]', '/test-one');
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been created.');

    // So path aliases are respected.
    $this->rebuildContainer();
    $node1 = $this->drupalGetNodeByTitle('One');
    $this->drupalGet('/node/add/page');
    $page->fillField('title[0][value]', 'Two');
    $page->fillField('body[0][value]', (string) $node1->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been created.');

    $targets = \Drupal::service('entity_usage.usage')->listTargets($this->drupalGetNodeByTitle('Two'));
    $this->assertCount(1, $targets);
    $this->assertCount(1, $targets['node']);
    $this->assertCount(1, $targets['node'][1]);
    $this->assertSame([
      "method" => "html_link",
      "field_name" => "body",
      "count" => "1",
    ], $targets['node'][1][0]);

    $this->drupalGet('/node/1/usage');
    $assert_session->linkExists('Two');
    $assert_session->linkByHrefExists('node/2');

    $this->drupalGet('/node/1/edit');
    $page->fillField('path[0][alias]', '/test-1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been updated.');

    $this->drupalGet('/node/2');
    $this->clickLink('Link');
    $assert_session->statusCodeEquals(200);

    $this->drupalGet('/node/1/usage');
    $assert_session->linkNotExists('Two');
    $assert_session->linkByHrefNotExists('node/2');
    $assert_session->linkExists('test-one');
    $assert_session->linkByHrefExists('redirect/edit/1');

    // So path aliases are respected.
    $this->rebuildContainer();
    $node1 = $this->drupalGetNodeByTitle('One');
    $node2 = $this->drupalGetNodeByTitle('Two');
    $this->drupalGet('/node/add/page');
    $page->fillField('title[0][value]', 'Three');
    $page->fillField('body[0][value]', "<p>" . $node1->toLink("Link", options: ['absolute' => TRUE])->toString() . "</p><p>" . $node2->toLink("Another link", options: ['absolute' => TRUE])->toString() . "</p>");
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been created.');

    $this->drupalGet('/node/1/edit');
    $page->fillField('path[0][alias]', '/test-v1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been updated.');

    // So path aliases are respected.
    $this->rebuildContainer();
    $node1 = $this->drupalGetNodeByTitle('One');
    $this->drupalGet('/node/add/page');
    $page->fillField('title[0][value]', 'Four');
    $page->fillField('body[0][value]', "<p>" . $node1->toLink("Link", options: ['absolute' => TRUE])->toString() . "</p><p>" . $node2->toLink("Another link", options: ['absolute' => TRUE])->toString() . "</p>");
    $page->pressButton('Save');
    $assert_session->pageTextContains('has been created.');

    $this->drupalGet('/node/2');
    $assert_session->linkExists('Link');
    $this->drupalGet('/node/3');
    $assert_session->linkExists('Link');
    $assert_session->linkExists('Another link');
    $this->drupalGet('/node/4');
    $assert_session->linkExists('Link');
    $assert_session->linkExists('Another link');

    $this->drupalGet('admin/config/content/link-remover');
    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
    ]));
    $this->assertSession()->buttonExists('Remove')->press();
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();

    $assert_session->pageTextContains('Updated one entity for references to One.');
    $assert_session->pageTextContains('Updated one entity for references to test-1.');
    $assert_session->pageTextContains('Updated one entity for references to test-one.');

    // Ensure we have not tried to edit the redirects.
    $assert_session->pageTextNotContains('Cannot save redirect');
    $this->drupalGet('/node/2');
    $assert_session->linkNotExists('Link');
    $this->drupalGet('/node/3');
    $assert_session->linkNotExists('Link');
    $assert_session->linkExists('Another link');
    $this->drupalGet('/node/4');
    $assert_session->linkNotExists('Link');
    $assert_session->linkExists('Another link');
  }

}
