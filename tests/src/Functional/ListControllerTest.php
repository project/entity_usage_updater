<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\entity_usage\Traits\EntityUsageLastEntityQueryTrait;

/**
 * Tests the tab listing the usage of a given entity with this module installed.
 *
 * @group entity_usage_updater
 */
class ListControllerTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  use EntityUsageLastEntityQueryTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'text',
    'path',
    'entity_usage_updater',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser([
      'access entity usage statistics',
      'administer nodes',
      'bypass node access',
    ]);
    $this->drupalLogin($account);

    $this->drupalCreateContentType(['type' => 'page']);

    // Set up the filter formats used by this test.
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a href> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();
    user_role_grant_permissions('authenticated', [$basic_html_format->getPermissionName()]);

    $current_request = \Drupal::request();
    $config = \Drupal::configFactory()->getEditable('entity_usage.settings');
    $config->set('site_domains', [$current_request->getHttpHost() . $current_request->getBasePath()]);
    $config->save();
    $this->config('entity_usage.settings')->set('local_task_enabled_entity_types', ['node'])->save();
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests the page listing the usage of entities.
   *
   * @covers \Drupal\entity_usage\Controller\ListUsageController::listUsagePage
   */
  public function testListController(): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // Create node 1.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 1 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $this->drupalGetNodeByTitle('Node 1');

    // Create node 2.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 2');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 2 has been created.');
    $node2 = $this->drupalGetNodeByTitle('Node 2');

    // Create node 3.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 3');
    $page->fillField('body[0][value]', (string) $node1->toLink(new FormattableMarkup("Link to %text", ['%text' => 'content']), options: ['absolute' => TRUE])->toString());
    // $page->fillField('body[0][value]', 'Testing!!!!!');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 3 has been created.');
    $node3 = $this->drupalGetNodeByTitle('Node 3');

    $this->assertSession()->linkByHrefExists($node1->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());

    $this->drupalGet('node/1/usage');
    $this->assertSession()->fieldExists('new_entity_id')->setValue($node2->id());
    $this->assertSession()->buttonExists('Update')->press();
    $this->assertSession()->pageTextContains('Are you sure you want to replace links to Node 1 with links to Node 2?');
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();

    // Check to see if the link has been replaced.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());

    $this->drupalGet('node/3/usage');
    // Ensure the form for updating is not present if there are no usages.
    $this->assertSession()->pageTextContains('There are no recorded usages for entity of type: node with id: 3');
    $this->assertSession()->fieldNotExists('new_entity_id');

    // Ensure that if usages only exist in old revisions then they are not
    // replaced even though the form is present.
    // @todo can we remove the form in if they are no current usages?
    $this->drupalGet('node/1/usage');
    $this->assertSession()->fieldExists('new_entity_id')->setValue($node2->id());
    $this->assertSession()->buttonExists('Update')->press();
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    // Check to see if the link has not been replaced.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());

    // Ensure the non-tab entity usage page works too.
    $this->drupalGet("admin/content/entity-usage/node/1");
    $this->assertSession()->fieldExists('new_entity_id')->setValue($node2->id());
    $this->assertSession()->buttonExists('Update')->press();
    $this->assertSession()->pageTextContains('Are you sure you want to replace links to Node 1 with links to Node 2?');
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    // Check to see if the link has been replaced.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());

    // Test removing a link and the cancel button on the form.
    $this->drupalGet("admin/content/entity-usage/node/2");
    $this->assertSession()->buttonExists('Remove')->press();
    $this->assertSession()->fieldNotExists('new_entity_id');
    $this->assertSession()->buttonExists('Cancel')->press();
    $this->assertSession()->fieldExists('new_entity_id');
    $this->assertSession()->buttonExists('Remove')->press();
    $this->assertSession()->pageTextContains('Are you sure you want to remove links to Node 2?');
    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    // Check to see if the link has been removed.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());
    // Ensure link content is still present.
    $this->assertSession()->responseContains("<div>Link to <em>content</em></div>");
  }

  /**
   * Tests the page listing the usage of entities access to updater form.
   *
   * @covers \Drupal\entity_usage\Controller\ListUsageController::listUsagePage
   */
  public function testListControllerAccess(): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $account = $this->drupalCreateUser(['create page content']);
    $this->drupalLogin($account);

    // Create node 1.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 1 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $this->drupalGetNodeByTitle('Node 1');

    // Create node 2.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 2');
    $page->fillField('body[0][value]', (string) $node1->toLink(new FormattableMarkup("Link to %text", ['%text' => 'content']), options: ['absolute' => TRUE])->toString());
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 2 has been created.');

    $this->drupalGet('node/1/usage');
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->drupalCreateUser(['access entity usage statistics']);
    $this->drupalLogin($account);
    $this->drupalGet('node/1/usage');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Entity usage information for Node 1');
    $this->assertSession()->buttonNotExists('Update');

    $account = $this->drupalCreateUser([
      'access entity usage statistics',
      'edit any page content',
    ]);
    $this->drupalLogin($account);
    $this->drupalGet('node/1/usage');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Entity usage information for Node 1');
    $this->assertSession()->buttonExists('Update');

    $account = $this->drupalCreateUser([
      'access entity usage statistics',
      'delete any page content',
    ]);
    $this->drupalLogin($account);
    $this->drupalGet('node/1/usage');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Entity usage information for Node 1');
    $this->assertSession()->buttonExists('Update');

    $account = $this->drupalCreateUser([
      'access entity usage statistics',
      'update referenced entities',
    ]);
    $this->drupalLogin($account);
    $this->drupalGet('node/1/usage');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Entity usage information for Node 1');
    $this->assertSession()->buttonExists('Update');
  }

}
