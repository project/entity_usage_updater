<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\link\LinkItemInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\entity_usage\Traits\EntityUsageLastEntityQueryTrait;

/**
 * Tests the Link Remover form.
 *
 * @group entity_usage_updater
 */
class LinkRemoverFormTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  use EntityUsageLastEntityQueryTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'text',
    'path',
    'link',
    'entity_usage_updater',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page']);

    // Create a field with settings to validate.
    $link_storage = FieldStorageConfig::create([
      'field_name' => 'test_links',
      'entity_type' => 'node',
      'type' => 'link',
      'cardinality' => 3,
      'settings' => [],
    ]);
    $link_storage->save();
    $link_field = FieldConfig::create([
      'field_storage' => $link_storage,
      'bundle' => 'page',
      'settings' => [
        'title' => DRUPAL_DISABLED,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ],
    ]);
    $link_field->save();
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'page')
      ->setComponent('test_links', [
        'type' => 'link_default',
      ])
      ->save();
    $display_repository->getViewDisplay('node', 'page')
      ->setComponent('test_links', ['type' => 'link'])
      ->save();

    // Set up the filter formats used by this test.
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a href> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();
    user_role_grant_permissions('authenticated', [$basic_html_format->getPermissionName(), 'access content']);

    $current_request = \Drupal::request();
    $config = \Drupal::configFactory()->getEditable('entity_usage.settings');
    $config->set('site_domains', [$current_request->getHttpHost() . $current_request->getBasePath()]);
    $config->save();
    $this->config('entity_usage.settings')->set('local_task_enabled_entity_types', ['node'])->save();
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests the link remover.
   *
   * @testWith [true]
   *           [false]
   *
   * @covers \Drupal\entity_usage_updater\Form\LinkRemoverForm
   *
   * @param bool $unpublish
   *   Whether to unpublish content.
   */
  public function testLinkRemover(bool $unpublish): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $account = $this->drupalCreateUser(['create page content']);
    $this->drupalLogin($account);

    // Create node 1.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 1 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $this->drupalGetNodeByTitle('Node 1');

    // Create node 2.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 2');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 2 has been created.');
    $node2 = $this->drupalGetNodeByTitle('Node 2');

    // Create node 3.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 3');
    $page->fillField('body[0][value]', (string) $node1->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->fillField('test_links[0][uri]', '/node/1');
    $page->fillField('test_links[1][uri]', 'http://example.org');
    $page->fillField('test_links[2][uri]', '/node/1');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 3 has been created.');
    $node3 = $this->drupalGetNodeByTitle('Node 3');
    $this->assertSession()->linkByHrefExists($node1->toUrl()->toString());
    $this->assertSession()->linkByHrefExists('http://example.org');

    // Create node 4.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 4');
    $page->fillField('body[0][value]', (string) $node2->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 4 has been created.');
    $node4 = $this->drupalGetNodeByTitle('Node 4');
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());

    // Create node 5.
    $this->drupalGet('node/add/page');
    $page->fillField('title[0][value]', 'Node 5');
    $page->fillField('body[0][value]', $node3->toLink("Link", options: ['absolute' => TRUE])->toString() . $node2->toLink("Link", options: ['absolute' => TRUE])->toString());
    $page->pressButton('Save');
    $assert_session->pageTextContains('Node 5 has been created.');
    $node5 = $this->drupalGetNodeByTitle('Node 5');
    $this->assertSession()->linkByHrefExists($node3->toUrl()->toString());
    $this->assertSession()->linkByHrefExists($node2->toUrl()->toString());

    $account = $this->drupalCreateUser(['update referenced entities']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/config/content/link-remover');
    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
      'not/a/link',
      $node2->toUrl()->setAbsolute()->toString(),
    ]));
    $this->assertSession()->buttonExists('Remove')->press();
    $this->assertSession()->pageTextContains('The following URLs can not be matched: not/a/link');

    $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
      $node1->toUrl()->setAbsolute()->toString(),
      $node2->toUrl()->setAbsolute()->toString(),
    ]));
    $this->assertSession()->fieldExists('unpublish')->setValue($unpublish);

    $this->assertSession()->buttonExists('Remove')->press();

    if ($unpublish) {
      $this->assertSession()->pageTextContains('You do not have permission to change.');
      $account = $this->drupalCreateUser([
        'update referenced entities',
        'edit any page content',
      ]);
      $this->drupalLogin($account);
      $this->drupalGet('admin/config/content/link-remover');
      $this->assertSession()->fieldExists('links')->setValue(implode("\n", [
        $node1->toUrl()->setAbsolute()->toString(),
        $node2->toUrl()->setAbsolute()->toString(),
      ]));
      $this->assertSession()->fieldExists('unpublish')->setValue($unpublish);

      $this->assertSession()->buttonExists('Remove')->press();
      $this->assertSession()->pageTextNotContains('You do not have permission to change.');
      $this->assertSession()->fieldExists('table[node|1][moderation]')->selectOption('unpublished');
      $this->assertSession()->fieldExists('table[node|2][moderation]')->selectOption('');
    }
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[1]', (string) $node1->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[2]', (string) $node1->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[3]', '1');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[1]/td[4]', 'Published');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[1]', (string) $node2->label());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[2]', (string) $node2->getEntityType()->getLabel());
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[3]', '2');
    $this->assertSession()->elementTextEquals('xpath', '//table[@id="edit-table"]/tbody/tr[2]/td[4]', 'Published');

    $this->assertSession()->buttonExists('Submit')->press();
    // Process the batch.
    $this->checkForMetaRefresh();
    $this->assertSession()->pageTextContains('Updated one entity for references to Node 1.');
    $this->assertSession()->pageTextContains('Updated 2 entities for references to Node 2.');
    if ($unpublish) {
      $this->assertSession()->pageTextContains('Updated moderation state for Node 1 to Unpublished.');
      $this->assertSession()->pageTextNotContains('Updated moderation state for Node 2 to Unpublished.');
    }

    // Check to see if the links have been removed.
    $this->drupalGet('node/3');
    $this->assertSession()->linkByHrefNotExists($node1->toUrl()->toString());
    $this->assertSession()->linkByHrefExists('http://example.org');

    $this->drupalGet('node/4');
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());

    // Check to see if the link node 2 has been removed but the link to node 3
    // is still there.
    $this->drupalGet('node/5');
    $this->assertSession()->linkByHrefNotExists($node2->toUrl()->toString());
    $this->assertSession()->linkByHrefExists($node3->toUrl()->toString());

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node_storage->resetCache();
    $node1 = $node_storage->load($node1->id());
    $node2 = $node_storage->load($node2->id());
    $this->assertSame(!$unpublish, $node1->isPublished(), 'Node 1 status is correct.');
    $this->assertTrue($node2->isPublished(), 'Node 2 is published.');
  }

  /**
   * Tests the link remover with content moderation installed but no config.
   *
   * @covers \Drupal\entity_usage_updater\Form\LinkRemoverForm
   */
  public function testContentModerationNoEffect(): void {
    // Just enabling the module should have no effect.
    \Drupal::service('module_installer')->install(['content_moderation']);
    $this->testLinkRemover(TRUE);
  }

}
