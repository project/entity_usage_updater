<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Tools;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;

/**
 * A test logger.
 */
class TestLogger implements LoggerInterface {

  /**
   * An array of log messages keyed by type.
   *
   * @var array
   */
  protected static array $logs;

  /**
   * TestLogger constructor.
   */
  public function __construct() {
    if (empty(static::$logs)) {
      $this->clear();
    }
  }

  /**
   * Returns an array of logs.
   *
   * @param string|false $level
   *   The log level to get logs for. FALSE returns all logs.
   *
   * @return array
   *   The array of log messages.
   */
  public function getLogs(string|false $level = FALSE): array {
    return FALSE === $level ? static::$logs : static::$logs[$level];
  }

  /**
   * Removes all log records.
   */
  public function clear(): void {
    static::$logs = [
      'emergency' => [],
      'alert' => [],
      'critical' => [],
      'error' => [],
      'warning' => [],
      'notice' => [],
      'info' => [],
      'debug' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    // Convert levels...
    static $map = [
      RfcLogLevel::DEBUG => 'debug',
      RfcLogLevel::INFO => 'info',
      RfcLogLevel::NOTICE => 'notice',
      RfcLogLevel::WARNING => 'warning',
      RfcLogLevel::ERROR => 'error',
      RfcLogLevel::CRITICAL => 'critical',
      RfcLogLevel::ALERT => 'alert',
      RfcLogLevel::EMERGENCY => 'emergency',
    ];

    foreach ($context as $key => $value) {
      if (ctype_alpha($key[0]) && !str_contains($message, $key)) {
        unset($context[$key]);
      }
    }

    $level = $map[$level] ?? $level;
    static::$logs[$level][] = PlainTextOutput::renderFromHtml(new FormattableMarkup($message, $context));
  }

  /**
   * {@inheritdoc}
   */
  public function emergency($message, array $context = []): void {
    $this->log('emergency', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function alert($message, array $context = []): void {
    $this->log('alert', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function critical($message, array $context = []): void {
    $this->log('critical', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function error($message, array $context = []): void {
    $this->log('error', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function warning($message, array $context = []): void {
    $this->log('warning', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function notice($message, array $context = []): void {
    $this->log('notice', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function info($message, array $context = []): void {
    $this->log('info', $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function debug($message, array $context = []): void {
    $this->log('debug', $message, $context);
  }

  /**
   * Registers the test logger to the container.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The ContainerBuilder to register the test logger to.
   */
  public static function register(ContainerBuilder $container): void {
    $container->register(__CLASS__)->addTag('logger');
  }

}
