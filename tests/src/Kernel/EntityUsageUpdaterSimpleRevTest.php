<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\entity_usage_updater\EntityUsageUpdater;

/**
 * Tests the batch update process with 'entity_test_rev' and revisions.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterSimpleRevTest extends EntityUsageUpdaterSimpleTest {

  /**
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected EntityStorageInterface $storage;

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_rev';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->assertInstanceOf(RevisionableStorageInterface::class, $this->storage);
  }

  /**
   * Tests that references in old revisions are ignored.
   */
  public function testRevisions(): void {
    $target_1 = $this->storage->create();
    $target_1->save();
    $target_2 = $this->storage->create();
    $target_2->save();
    $target_3 = $this->storage->create();
    $target_3->save();

    $new_target_1 = $this->storage->create();
    $new_target_1->save();
    $new_target_3 = $this->storage->create();
    $new_target_3->save();

    $entity = $this->storage->create();
    assert($entity instanceof RevisionableInterface);
    $entity->set('field_reference', [
      ['target_id' => $target_1->id()],
      ['target_id' => $target_2->id()],
      ['target_id' => $target_3->id()],
    ]);

    $entity->save();
    $vid = $entity->getRevisionId();

    $new_revision = $this->storage->createRevision($entity, TRUE);

    // Now make a new revision with different values.
    $new_revision->set('field_reference', NULL);
    $new_revision->save();

    $updater = new EntityUsageUpdater();
    $updater->update([
      static::$entityTypeId => [
        $target_1->id() => [static::$entityTypeId, $new_target_1->id()],
        $target_3->id() => [static::$entityTypeId, $new_target_3->id()],
      ],
    ]);

    $this->batchProcess();

    $entity = $this->reloadEntity($entity);
    $this->assertTrue($entity->get('field_reference')->isEmpty());

    // Confirm that the old revision remains unaffected. (I'm not sure it's
    // actually possible to update the old revision data, but just in case...)
    $revision = $this->storage->loadRevision($vid);

    $this->assertSame($target_1->id(), $revision->field_reference[0]->target_id);
    $this->assertSame($target_2->id(), $revision->field_reference[1]->target_id);
    $this->assertSame($target_3->id(), $revision->field_reference[2]->target_id);
  }

}
