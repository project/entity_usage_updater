<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Tests updating references in fields on multiple paragraphs with links.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterMultipleParagraphsTest extends EntityUsageUpdaterKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs',
    'entity_reference_revisions',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_rev';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('paragraph');

    $config = $this->config('entity_usage.settings');
    $config->set('track_enabled_source_entity_types', ['paragraph']);
    $config->set('track_enabled_target_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_plugins', ['html_link']);
    $config->save();
  }

  /**
   * Tests updating references in a field within a nested paragraph.
   *
   * As well as updating the target field it should update the two entity
   * reference revision fields referencing the paragraphs so that the update
   * takes effect.
   */
  public function testMultipleParagraphs(): void {
    $paragraph_type = ParagraphsType::create([
      'label' => 'Test Paragraph Type',
      'id' => 'test',
    ]);
    $paragraph_type->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_text',
      'entity_type' => 'paragraph',
      'type' => 'text',
      'cardinality' => 1,
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $paragraph_type->id(),
      'label' => 'Text field',
    ])->save();

    // Add a paragraph field to entity_test.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_paragraph',
      'entity_type' => static::$entityTypeId,
      'type' => 'entity_reference_revisions',
      'cardinality' => '-1',
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ]);

    // Ensure this field is not translatable to test the preSave override in
    // \Drupal\entity_usage_updater\EntityReferenceRevisionsItem.
    $field_storage->setTranslatable(FALSE);

    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => static::$entityTypeId,
    ])->save();

    $target_1 = $this->storage->create();
    $target_1->save();
    $target_2 = $this->storage->create();
    $target_2->save();

    $paragraph_storage = $this->container->get('entity_type.manager')
      ->getStorage('paragraph');
    $paragraph1 = $paragraph_storage->create(['type' => 'test']);
    $paragraph1->field_text->value = $target_1->toLink('Link 1')->toString();
    $paragraph1->save();
    $paragraph2 = $paragraph_storage->create(['type' => 'test']);
    $paragraph2->field_text->value = $target_2->toLink('Link 2')->toString();
    $paragraph2->save();
    $paragraph3 = $paragraph_storage->create(['type' => 'test']);
    $paragraph3->field_text->value = $target_1->toLink('Link 3')->toString();
    $paragraph3->save();

    $host = $this->storage->create();
    $host->field_paragraph = [
      $paragraph1, $paragraph2, $paragraph3,
    ];
    $host->save();

    // Ensure we've only created the necessary number of revisions.
    $this->assertSame(1, $this->countRevisions($paragraph1));
    $this->assertSame(1, $this->countRevisions($paragraph2));
    $this->assertSame(1, $this->countRevisions($paragraph3));
    $this->assertSame(1, $this->countRevisions($host));

    $updater = new EntityUsageUpdater();
    $updater->remove([
      static::$entityTypeId => [
        $target_1->id(),
      ],
    ]);
    $this->batchProcess();

    $host = $this->reloadEntity($host);
    $this->assertSame($host->field_paragraph[0]->entity->field_text->value, 'Link 1');
    $this->assertSame($host->field_paragraph[1]->entity->field_text->value, $target_2->toLink('Link 2')->toString()->getGeneratedLink());
    $this->assertSame($host->field_paragraph[2]->entity->field_text->value, 'Link 3');

    // Ensure we've only created the necessary number of revisions.
    $this->assertSame(2, $this->countRevisions($paragraph1));
    $this->assertSame(2, $this->countRevisions($paragraph2));
    $this->assertSame(2, $this->countRevisions($paragraph3));
    $this->assertSame(2, $this->countRevisions($host));
  }

}
