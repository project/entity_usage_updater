<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Entity\TranslatableRevisionableInterface;
use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the batch update process with 'entity_mul_rev' and translations.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterSimpleMulRevTest extends EntityUsageUpdaterSimpleRevTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language'];

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_mulrev';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['language']);
    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('de')->save();
  }

  /**
   * Tests updating an entity with multiple translations.
   */
  public function testMultilingual(): void {
    // Set up an entity with three translations, all of which start off
    // referencing targets, but then one gets emptied.
    $target_1 = $this->storage->create();
    $target_1->save();
    $target_2 = $this->storage->create();
    $target_2->save();
    $target_3 = $this->storage->create();
    $target_3->save();

    $new_target_1 = $this->storage->create();
    $new_target_1->save();
    $new_target_3 = $this->storage->create();
    $new_target_3->save();

    $entity = $this->storage->create();
    assert($entity instanceof TranslatableRevisionableInterface);
    $entity->set('field_reference', [
      ['target_id' => $target_1->id()],
      ['target_id' => $target_2->id()],
      ['target_id' => $target_3->id()],
    ]);

    $entity->addTranslation('fr', $entity->toArray());
    $entity->addTranslation('de', $entity->toArray());
    $entity->save();

    $new_revision = $this->storage->createRevision($entity, TRUE);
    assert($new_revision instanceof TranslatableRevisionableInterface);
    $french_new_revision = $new_revision->getTranslation('fr');
    $this->assertSame('fr', $french_new_revision->language()->getId());
    $french_new_revision->set('field_reference', NULL)
      ->save();

    $updater = new EntityUsageUpdater();
    $updater->update([
      static::$entityTypeId => [
        $target_1->id() => [static::$entityTypeId, $new_target_1->id()],
        $target_3->id() => [static::$entityTypeId, $new_target_3->id()],
      ],
    ]);
    $this->batchProcess();

    $entity = $this->reloadEntity($entity);
    assert($entity instanceof TranslatableInterface);
    $this->assertSame('en', $entity->language()->getId());

    $this->assertSame($new_target_1->id(), $entity->field_reference[0]->target_id);
    $this->assertSame($target_2->id(), $entity->field_reference[1]->target_id);
    $this->assertSame($new_target_3->id(), $entity->field_reference[2]->target_id);

    $german_entity = $entity->getTranslation('de');
    $this->assertSame('de', $german_entity->language()->getId());
    $this->assertSame($new_target_1->id(), $german_entity->field_reference[0]->target_id);
    $this->assertSame($target_2->id(), $german_entity->field_reference[1]->target_id);
    $this->assertSame($new_target_3->id(), $german_entity->field_reference[2]->target_id);

    $french_entity = $entity->getTranslation('fr');
    $this->assertSame('fr', $french_entity->language()->getId());
    $this->assertTrue($french_entity->field_reference->isEmpty());
  }

}
