<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\Entity\RevisionableInterface;
use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Tests updating references in fields on paragraphs.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterParagraphsTest extends EntityUsageUpdaterKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs',
    'entity_reference_revisions',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_rev';

  /**
   * Flag for whether the entity is revisionable.
   *
   * @var bool
   */
  protected bool $isRevisionable;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('paragraph');

    $config = $this->config('entity_usage.settings');
    $config->set('track_enabled_source_entity_types', ['paragraph']);
    $config->set('track_enabled_target_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_plugins', ['entity_reference']);
    $config->save();
    $this->isRevisionable = $this->container->get('entity_type.manager')->getDefinition(static::$entityTypeId)->isRevisionable();
  }

  /**
   * Tests updating references in a field within a nested paragraph.
   *
   * As well as updating the target field it should update the two entity
   * reference revision fields referencing the paragraphs so that the update
   * takes effect.
   */
  public function testNested(): void {
    $paragraph_type = ParagraphsType::create([
      'label' => 'Test Paragraph Type',
      'id' => 'test',
    ]);
    $paragraph_type->save();

    $paragraph_type_nested = ParagraphsType::create([
      'label' => 'Test Nested Paragraph Type',
      'id' => 'test_nested',
    ]);
    $paragraph_type_nested->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_reference',
      'type' => 'entity_reference',
      'entity_type' => 'paragraph',
      'cardinality' => 3,
      'settings' => [
        'target_type' => static::$entityTypeId,
      ],
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_nested',
      'label' => 'Reference field',
    ])->save();

    // Add a paragraph field for the nested paragraph.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'nested_paragraph_field',
      'entity_type' => 'paragraph',
      'type' => 'entity_reference_revisions',
      'cardinality' => '-1',
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test',
    ])->save();

    // Add a paragraph field to entity_test.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_paragraph',
      'entity_type' => static::$entityTypeId,
      'type' => 'entity_reference_revisions',
      'cardinality' => '-1',
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => static::$entityTypeId,
    ])->save();

    $target_1 = $this->storage->create();
    $target_1->save();
    $target_2 = $this->storage->create();
    $target_2->save();
    $target_3 = $this->storage->create();
    $target_3->save();

    $new_target_1 = $this->storage->create();
    $new_target_1->save();
    $new_target_3 = $this->storage->create();
    $new_target_3->save();

    $paragraph_storage = $this->container->get('entity_type.manager')
      ->getStorage('paragraph');
    $nested_paragraph = $paragraph_storage->create(['type' => 'test_nested']);
    assert($nested_paragraph instanceof RevisionableInterface);
    $nested_paragraph->set('field_reference', [
      ['target_id' => $target_1->id()],
      ['target_id' => $target_2->id()],
      ['target_id' => $target_3->id()],
    ]);
    $nested_paragraph->save();

    $outer_paragraph = $paragraph_storage->create(['type' => 'test']);
    assert($outer_paragraph instanceof RevisionableInterface);
    $outer_paragraph->set('nested_paragraph_field', [
      'target_id' => $nested_paragraph->id(),
      'target_revision_id' => $nested_paragraph->getRevisionId(),
    ]);
    $outer_paragraph->save();

    $host = $this->storage->create();
    $host->set('field_paragraph', [
      'target_id' => $outer_paragraph->id(),
      'target_revision_id' => $outer_paragraph->getRevisionId(),
    ]);
    $host->save();

    if ($this->isRevisionable) {
      // Ensure we've only created the necessary number of revisions.
      $this->assertSame(1, $this->countRevisions($host->field_paragraph->entity));
      $this->assertSame(1, $this->countRevisions($host->field_paragraph->entity->nested_paragraph_field->entity));
      $this->assertSame(1, $this->countRevisions($host));
    }

    $updater = new EntityUsageUpdater();
    $updater->update([
      static::$entityTypeId => [
        $target_1->id() => [static::$entityTypeId, $new_target_1->id()],
        $target_3->id() => [static::$entityTypeId, $new_target_3->id()],
      ],
    ]);
    $this->batchProcess();

    $host = $this->reloadEntity($host);

    $this->assertSame($new_target_1->id(), $host->field_paragraph->entity->nested_paragraph_field->entity->field_reference[0]->target_id);
    $this->assertSame($target_2->id(), $host->field_paragraph->entity->nested_paragraph_field->entity->field_reference[1]->target_id);
    $this->assertSame($new_target_3->id(), $host->field_paragraph->entity->nested_paragraph_field->entity->field_reference[2]->target_id);

    if ($this->isRevisionable) {
      // Ensure we've only created the necessary number of revisions. There are 3
      // revisions because we have 2 replacements and each replacement creates
      // its own set of revisions.
      $this->assertSame(3, $this->countRevisions($host->field_paragraph->entity));
      $this->assertSame(3, $this->countRevisions($host->field_paragraph->entity->nested_paragraph_field->entity));
      $this->assertSame(3, $this->countRevisions($host));
    }
  }

}
