<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Provides a base class for testing the update process.
 */
abstract class EntityUsageUpdaterKernelTestBase extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['entity_usage_updater', 'entity_usage'];

  /**
   * The entity type ID of the entity that will have a field attached.
   *
   * @var string
   */
  protected static string $entityTypeId = 'entity_test';

  /**
   * Storage for the entity that will have a field attached.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['entity_usage']);
    $this->installSchema('entity_usage', ['entity_usage']);
    $this->installEntitySchema(static::$entityTypeId);
    $this->storage = $this->container->get('entity_type.manager')
      ->getStorage(static::$entityTypeId);

    // Create user 1 so entities are valid.
    $storage = $this->container->get('entity_type.manager')->getStorage('user');
    $storage
      ->create([
        'uid' => 1,
        'name' => 'entity-test',
        'mail' => 'entity@localhost',
        'status' => TRUE,
      ])
      ->save();

  }

  /**
   * Executes batches until there are none left.
   */
  protected function batchProcess(): void {
    do {
      $batch = &batch_get();
      assert($batch);
      $batch['progressive'] = FALSE;
      batch_process();
    } while ($batch);
  }

  /**
   * Gets the number of revisions an entity has.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to count revisions for.
   *
   * @return int
   *   The number of revisions an entity has.
   */
  protected function countRevisions(EntityInterface $entity): int {
    $storage = $this->container->get('entity_type.manager')->getStorage($entity->getEntityTypeId());
    return (int) $storage->getQuery()->accessCheck(FALSE)->allRevisions()->condition('id', $entity->id())->count()->execute();
  }

}
