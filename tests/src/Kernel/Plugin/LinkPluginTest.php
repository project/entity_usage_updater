<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

/**
 * Provides tests for the link plugin.
 *
 * @group entity_usage_updater
 */
class LinkPluginTest extends PluginKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['link'];

  /**
   * {@inheritdoc}
   */
  protected static string $fieldType = 'link';

  /**
   * {@inheritdoc}
   */
  protected static string $pluginId = 'link';

  /**
   * Tests updating a link field.
   */
  public function testUpdate(): void {
    $target = $this->storage->create();
    $target->save();
    $this->entity->set('test_field', ['uri' => $target->toUrl()]);

    $this->plugin->update($target, static::$entityTypeId, 42, $this->entity->get('test_field')[0]);

    $this->assertSame('entity:entity_test/42', $this->entity->get('test_field')->uri);
  }

  /**
   * Tests removing a link field.
   */
  public function testRemove(): void {
    $target = $this->storage->create();
    $target->save();
    $this->entity->set('test_field', ['uri' => $target->toUrl()]);

    $this->plugin->remove($target, $this->entity->get('test_field')[0]);

    $this->assertNull($this->entity->get('test_field')->uri);
  }

}
