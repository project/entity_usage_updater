<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Provides a base class for testing the html_link plugin.
 */
abstract class HtmlLinkPluginTestBase extends PluginKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language', 'content_translation', 'locale'];

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_mul';

  /**
   * {@inheritdoc}
   */
  protected static string $fieldType = 'text';

  /**
   * {@inheritdoc}
   */
  protected static string $pluginId = 'html_link';

  /**
   * An associative array of languages used in the test keyed by language code.
   *
   * @var \Drupal\language\ConfigurableLanguageInterface[]
   */
  protected array $languages;

  /**
   * {@inheritdoc}
   */
  protected function preFieldSetup(): void {
    // @see https://drupal.stackexchange.com/q/295461
    $this->installConfig(['language']);
    $this->installEntitySchema('configurable_language');

    $this->languages['fr'] = ConfigurableLanguage::createFromLangcode('fr');
    $this->languages['fr']->save();

    $config = $this->config('language.negotiation');
    $config->set('url.prefixes', [
      'en' => '',
      'fr' => 'fr',
    ])->save();

    $user = $this->createUser();
    $this->container->get('language_negotiator')->setCurrentUser($user);
  }

}
