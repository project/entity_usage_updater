<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

/**
 * Provides tests for the html_link plugin configured to convert to LinkIt.
 *
 * @group entity_usage_updater
 */
class HtmlLinkPluginLinkItTest extends HtmlLinkPluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected function preFieldSetup(): void {
    parent::preFieldSetup();
    $this->pluginConfig = [
      'convert_to_linkit' => TRUE,
    ];
  }

  /**
   * Provides test data for update.
   */
  public static function updateLinkTexts(): iterable {
    yield  [
      '<p>Some text with <a href="%%ENTITY_URL%%?foo=bar#fragment">a link</a>.</p>',
      '<p>Some text with <a data-entity-substitution="canonical" data-entity-type="entity_test_mul" data-entity-uuid="%%ENTITY_UUID%%" href="%%ENTITY_URL%%?foo=bar#fragment">a link</a>.</p>',
    ];
    yield  [
      '<p>Invalid markup (missing closing p tag).<p>Some text with <a href="%%ENTITY_URL%%?foo=bar#fragment">a link</a>.</p>',
      '<p>Invalid markup (missing closing p tag).<p>Some text with <a data-entity-substitution="canonical" data-entity-type="entity_test_mul" data-entity-uuid="%%ENTITY_UUID%%" href="%%ENTITY_URL%%?foo=bar#fragment">a link</a>.</p>',
    ];
  }

  /**
   * Tests updating a link in a text field and converting it to a LinkIt link.
   *
   * @dataProvider updateLinkTexts
   */
  public function testUpdate(string $initial, string $expected): void {
    $target = $this->storage->create();
    $target->save();
    $new_entity = $this->storage->create();
    $new_entity->save();
    $initial_text = str_replace('%%ENTITY_URL%%', $target->toUrl()->toString(), $initial);
    $this->entity->set('test_field', $initial_text);

    $this->plugin->update($target, static::$entityTypeId, $new_entity->id(), $this->entity->get('test_field')[0]);

    $search = [
      '%%ENTITY_UUID%%',
      '%%ENTITY_URL%%',
    ];
    $replace = [
      $new_entity->uuid(),
      $new_entity->toUrl()->toString(),
    ];
    $expected_text = str_replace($search, $replace, $expected);
    $this->assertSame($expected_text, $this->entity->get('test_field')->value);
  }

}
