<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

use Drupal\Core\Entity\TranslatableInterface;

/**
 * Provides tests for the html_link plugin.
 *
 * @group entity_usage_updater
 */
class HtmlLinkPluginTest extends HtmlLinkPluginTestBase {

  protected function preFieldSetup(): void {
    parent::preFieldSetup();
    $this->pluginConfig = [
      'convert_to_linkit' => FALSE,
    ];
  }

  /**
   * Provides test data.
   */
  public static function linkTexts(): array {
    return [
      ['<p>Some text with <a href="%%ENTITY_URL%%">a link</a>.</p>'],
      ['<p>Invalid markup (missing closing p tag).<p>Some text with <a href="%%ENTITY_URL%%">a link</a>.</p>'],
    ];
  }

  /**
   * Tests updating a link in a text field.
   *
   * @dataProvider linkTexts
   */
  public function testUpdate(string $text): void {
    $target = $this->storage->create();
    $target->save();
    $new_entity = $this->storage->create();
    $new_entity->save();
    $initial_text = str_replace('%%ENTITY_URL%%', $target->toUrl()->toString(), $text);
    $this->entity->set('test_field', $initial_text);

    $this->plugin->update($target, static::$entityTypeId, $new_entity->id(), $this->entity->get('test_field')[0]);

    $expected = str_replace('%%ENTITY_URL%%', $new_entity->toUrl()->toString(), $text);
    $this->assertSame($expected, $this->entity->get('test_field')->value);
  }

  /**
   * Ensures a language-prefixed URL maintains its prefix after update.
   *
   * @dataProvider linkTexts
   */
  public function testUpdateTranslation(string $text): void {
    $this->entityTypeManager->getStorage('language_content_settings')->create([
      'target_entity_type_id' => static::$entityTypeId,
      'target_bundle' => static::$entityTypeId,
    ])->save();
    $this->container->get('content_translation.manager')
      ->setEnabled(static::$entityTypeId, static::$entityTypeId, TRUE);

    $target = $this->storage->create();
    assert($target instanceof TranslatableInterface);
    $french_translation = $target->addTranslation('fr', $target->toArray());
    $this->assertSame('fr', $french_translation->language()->getId());
    $target->save();

    $new_entity = $this->storage->create();
    $french_new_entity = $new_entity->addTranslation('fr', $new_entity->toArray());
    $this->assertSame('fr', $french_new_entity->language()->getId());
    $new_entity->save();

    $options = ['language' => $this->languages['fr']];
    $target_url = $target->toUrl('canonical', $options)->toString();
    $initial_text = str_replace('%%ENTITY_URL%%', $target_url, $text);
    $this->entity->set('test_field', $initial_text);

    $this->plugin->update($target, static::$entityTypeId, $new_entity->id(), $this->entity->get('test_field')[0]);

    $new_reference_url = $new_entity->toUrl('canonical', $options)->toString();
    $expected = str_replace('%%ENTITY_URL%%', $new_reference_url, $text);
    $this->assertSame($expected, $this->entity->get('test_field')->value);
  }

  /**
   * Provides test data for remove.
   */
  public static function removeLinkTexts(): iterable {
    yield 'Correct markup with link' => [
      '<p>Some text with <a href="%%ENTITY_URL%%?foo=bar#fragment">a link</a>.</p>',
      '<p>Some text with a link.</p>',
    ];
    // Note that because we use \DomDocument to manipulate the HTML, we end up
    // fixing any broken HTML unlike link replacement.
    yield 'Broken markup with link' => [
      '<p>Invalid markup (missing closing p tag).<p>Some text with <a href="%%ENTITY_URL%%?foo=bar#fragment">a <em>link</em></a>.</p>',
      '<p>Invalid markup (missing closing p tag).</p><p>Some text with a <em>link</em>.</p>',
    ];

    yield 'Broken markup no link' => [
      '<p>Invalid markup (missing closing p tag).',
      '<p>Invalid markup (missing closing p tag).',
    ];

  }

  /**
   * Tests removing a link in a text field.
   *
   * @dataProvider removeLinkTexts
   */
  public function testRemove(string $initial, string $expected): void {
    $target = $this->storage->create();
    $target->save();
    $new_entity = $this->storage->create();
    $new_entity->save();
    $initial_text = str_replace('%%ENTITY_URL%%', $target->toUrl()->toString(), $initial);
    $this->entity->set('test_field', $initial_text);

    $this->plugin->remove($target, $this->entity->get('test_field')[0]);

    $this->assertSame($expected, $this->entity->get('test_field')->value);
  }

}
