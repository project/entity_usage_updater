<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

/**
 * Provides tests for updating entity reference fields.
 *
 * @group entity_usage_updater
 */
class EntityReferencePluginTest extends PluginKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static string $fieldType = 'entity_reference';

  /**
   * {@inheritdoc}
   */
  protected static string $pluginId = 'entity_reference';

  /**
   * Tests updating an entity reference field.
   */
  public function testUpdate(): void {
    $target = $this->storage->create();
    $target->save();
    $this->entity->set('test_field', ['target_id' => $target->id()]);

    $this->plugin->update($target, static::$entityTypeId, 42, $this->entity->get('test_field')[0]);

    $this->assertSame(42, $this->entity->get('test_field')->target_id);
  }

  /**
   * Tests removing an entity reference field.
   */
  public function testRemove(): void {
    $target = $this->storage->create();
    $target->save();
    $this->entity->set('test_field', ['target_id' => $target->id()]);

    $this->plugin->remove($target, $this->entity->get('test_field')[0]);

    $this->assertNull($this->entity->get('test_field')->target_id);
  }

}
