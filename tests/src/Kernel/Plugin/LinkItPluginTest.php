<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

/**
 * Provides tests for the linkit plugin.
 *
 * @group entity_usage_updater
 */
class LinkItPluginTest extends PluginKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static string $fieldType = 'text';

  /**
   * {@inheritdoc}
   */
  protected static string $pluginId = 'linkit';

  /**
   * Tests updating a LinkIt link within a text field.
   */
  public function testUpdate(): void {
    $target = $this->storage->create();
    $target->save();
    $new_entity = $this->storage->create();
    $new_entity->save();
    $options = ['query' => ['foo' => 'bar'], 'fragment' => 'fragment'];
    $href = $target->toUrl('canonical', $options)->toString();
    $this->entity->set('test_field', '<p>Some text with <a data-entity-substitution="canonical" data-entity-type="' . $target->getEntityTypeId() . '" data-entity-uuid="' . $target->uuid() . '" href="' . $href . '">' . 'a link</a>.</p>');

    $this->plugin->update($target, $new_entity->getEntityTypeId(), $new_entity->id(), $this->entity->get('test_field')[0]);

    $expected = '<p>Some text with <a data-entity-substitution="canonical" data-entity-type="' . $new_entity->getEntityTypeId() . '" data-entity-uuid="' . $new_entity->uuid() . '" href="' . $href . '">' . 'a link</a>.</p>';
    $this->assertSame($expected, $this->entity->get('test_field')->value);
  }

  /**
   * Tests remove a LinkIt link within a text field.
   */
  public function testRemove(): void {
    $target = $this->storage->create();
    $target->save();
    $new_entity = $this->storage->create();
    $new_entity->save();
    $options = ['query' => ['foo' => 'bar'], 'fragment' => 'fragment'];
    $href = $target->toUrl('canonical', $options)->toString();
    $this->entity->set('test_field', '<p>Some text with <a data-entity-substitution="canonical" data-entity-type="' . $target->getEntityTypeId() . '" data-entity-uuid="' . $target->uuid() . '" href="' . $href . '">' . 'a link</a>.</p>');

    $this->plugin->remove($target, $this->entity->get('test_field')[0]);

    $this->assertSame('<p>Some text with a link.</p>', $this->entity->get('test_field')->value);
  }

}
