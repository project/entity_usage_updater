<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterPluginInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\entity_usage_updater\Kernel\EntityUsageUpdaterKernelTestBase;

/**
 * Provides a base class for testing entity usage updater plugins.
 */
abstract class PluginKernelTestBase extends EntityUsageUpdaterKernelTestBase {

  /**
   * The field type that will have its contents updated by the plugin.
   *
   * @var string
   */
  protected static string $fieldType;

  /**
   * The ID of the plugin under test.
   *
   * @var string
   */
  protected static string $pluginId;

  /**
   * The entity that will have a field attached.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * The configuration to pass into the plugin under test.
   *
   * @var array
   */
  protected array $pluginConfig = [];

  /**
   * The plugin under test.
   *
   * @var \Drupal\entity_usage_updater\EntityUsageUpdaterPluginInterface
   */
  protected EntityUsageUpdaterPluginInterface $plugin;

  /**
   * Does any necessary setup prior to creating the fields.
   */
  protected function preFieldSetup(): void {}

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->preFieldSetup();
    $this->installEntitySchema(static::$entityTypeId);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'test_field',
      'type' => static::$fieldType,
      'entity_type' => static::$entityTypeId,
      'cardinality' => 1,
      'settings' => [
        'target_type' => static::$entityTypeId,
      ],
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => static::$entityTypeId,
      'label' => 'Test field',
    ])->save();

    $this->storage = $this->container->get('entity_type.manager')->getStorage(static::$entityTypeId);
    $this->entity = $this->storage->create();
    $plugin_manager = $this->container->get('plugin.manager.entity_usage_updater');
    assert($plugin_manager instanceof PluginManagerInterface);
    $this->plugin = $plugin_manager->createInstance(static::$pluginId, $this->pluginConfig);
  }

}
