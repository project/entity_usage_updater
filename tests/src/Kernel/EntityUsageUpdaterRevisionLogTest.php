<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the batch update process with 'entity_test_rev' and revisions.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterRevisionLogTest extends EntityUsageUpdaterKernelTestBase {

  /**
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected EntityStorageInterface $storage;

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test_mulrev_changed_rev';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->config('entity_usage.settings');
    $config->set('track_enabled_source_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_target_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_plugins', ['entity_reference']);
    $config->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_reference',
      'type' => 'entity_reference',
      'entity_type' => static::$entityTypeId,
      'cardinality' => 3,
      'settings' => [
        'target_type' => static::$entityTypeId,
      ],
      'translatable' => TRUE,
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => static::$entityTypeId,
      'label' => 'Test field',
    ])->save();
  }

  /**
   * Tests updating an entity reference field.
   */
  public function testUpdate(): void {
    $target_1 = $this->storage->create();
    $target_1->save();
    $target_2 = $this->storage->create();
    $target_2->save();
    $target_3 = $this->storage->create();
    $target_3->save();

    $new_target_1 = $this->storage->create();
    $new_target_1->save();
    $new_target_3 = $this->storage->create();
    $new_target_3->save();

    $entity = $this->storage->create();
    $entity->set('field_reference', [
      ['target_id' => $target_1->id()],
      ['target_id' => $target_2->id()],
      ['target_id' => $target_3->id()],
    ]);
    $entity->save();

    $updater = new EntityUsageUpdater();
    $updater->update([
      static::$entityTypeId => [
        $target_1->id() => [static::$entityTypeId, $new_target_1->id()],
        $target_3->id() => [static::$entityTypeId, $new_target_3->id()],
      ],
    ]);
    $this->batchProcess();

    $entity = $this->reloadEntity($entity);
    $this->assertSame($new_target_1->id(), $entity->field_reference[0]->target_id);
    $this->assertSame($target_2->id(), $entity->field_reference[1]->target_id);
    $this->assertSame($new_target_3->id(), $entity->field_reference[2]->target_id);
    $this->assertCount(3, $entity->field_reference);

    $this->assertInstanceOf(RevisionLogInterface::class, $entity);
    $this->assertSame('Automatically updated by the entity usage updater module to replace entity_test_mulrev_changed_rev 3 with entity_test_mulrev_changed_rev 5', $entity->getRevisionLogMessage());

    $updater->remove([
      static::$entityTypeId => [
        $new_target_1->id(),
        $new_target_3->id(),
      ],
    ]);
    $this->batchProcess();

    $entity = $this->reloadEntity($entity);
    $this->assertSame($target_2->id(), $entity->field_reference[0]->target_id);
    $this->assertCount(1, $entity->field_reference);

    $this->assertInstanceOf(RevisionLogInterface::class, $entity);
    $this->assertSame('Automatically updated by the entity usage updater module to remove entity_test_mulrev_changed_rev 5', $entity->getRevisionLogMessage());

  }

}
