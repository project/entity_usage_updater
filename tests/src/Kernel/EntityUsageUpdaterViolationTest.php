<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\entity_usage_updater\Tools\TestLogger;
use Drupal\user\Entity\User;

/**
 * Tests the batch update process with 'entity_test'.
 *
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterViolationTest extends EntityUsageUpdaterKernelTestBase {

  /**
   * The field storage.
   */
  private FieldStorageConfig $fieldStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);

    $config = $this->config('entity_usage.settings');
    $config->set('track_enabled_source_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_target_entity_types', [static::$entityTypeId]);
    $config->set('track_enabled_plugins', ['entity_reference']);
    $config->save();

    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_reference',
      'type' => 'entity_reference',
      'entity_type' => static::$entityTypeId,
      'cardinality' => 3,
      'settings' => [
        'target_type' => static::$entityTypeId,
      ],
      'translatable' => TRUE,
    ]);
    $this->fieldStorage->save();

    FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => static::$entityTypeId,
      'label' => 'Test field',
      'required' => TRUE,
    ])->save();
  }

  public function register(ContainerBuilder $container): void {
    parent::register($container);
    TestLogger::register($container);
  }

  /**
   * Tests updating an entity reference field.
   */
  public function testUpdate(): void {
    $logger = $this->container->get(TestLogger::class);
    $target_1 = $this->storage->create();
    $target_1->save();

    $new_target_1 = $this->storage->create();
    $new_target_1->save();

    $entity = $this->storage->create();
    $entity->set('field_reference', [
      ['target_id' => $target_1->id()],
    ]);
    $entity->save();

    $updater = new EntityUsageUpdater();
    $updater->remove([
      static::$entityTypeId => [
        $target_1->id(),
      ],
    ]);
    $this->batchProcess();

    // Test log message.
    $logs = $logger->getLogs('error');
    $this->assertCount(1, $logs);
    $this->assertSame('Cannot save entity_test 3 due to: field_reference=This value should not be null.', $logs[0]);

    // Test messenger message.
    $messages = $this->container->get('messenger')->messagesByType('error');
    $this->assertCount(1, $messages);
    $this->assertInstanceOf(Markup::class, $messages[0]);
    $this->assertSame('Cannot save <em class="placeholder">entity_test</em> <em class="placeholder">3</em> because: This value should not be null.', (string) $messages[0]);

    // Ensure the value has not changed.
    $entity = $this->reloadEntity($entity);
    // This will not have changed.
    $this->assertSame($target_1->id(), $entity->field_reference[0]->target_id);

    // Make the entity invalid already.
    assert($entity instanceof FieldableEntityInterface);
    $this->assertCount(0, $entity->validate());
    User::load(1)->delete();
    $this->assertCount(1, $entity->validate());

    $logger->clear();
    $updater->remove([
      static::$entityTypeId => [
        $target_1->id(),
      ],
    ]);
    $this->batchProcess();
    $logs = $logger->getLogs('error');
    $this->assertCount(1, $logs);
    $this->assertSame('Cannot save entity_test 3 due to: user_id.0.target_id=The referenced entity (<em class="placeholder">user</em>: <em class="placeholder">1</em>) does not exist., field_reference=This value should not be null.', $logs[0]);

    // Ensure the value has not changed.
    $entity = $this->reloadEntity($entity);
    // This will not have changed.
    $this->assertSame($target_1->id(), $entity->field_reference[0]->target_id);

    // Test that we can update invalid entities if we don't add to the
    // violations.
    $logger->clear();
    $updater->update([
      static::$entityTypeId => [
        $target_1->id() => [static::$entityTypeId, $new_target_1->id()],
      ],
    ]);
    $this->batchProcess();
    $logs = $logger->getLogs('error');
    $this->assertCount(0, $logs);
    $this->assertSame($new_target_1->id(), $entity->field_reference[0]->target_id);
  }

}
