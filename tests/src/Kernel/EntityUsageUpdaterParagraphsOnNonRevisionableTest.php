<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_usage_updater\Kernel;

/**
 * Tests updating references in paragraph field on non revisionable entities.
 *
 * @group entity_usage_updater
 */
class EntityUsageUpdaterParagraphsOnNonRevisionableTest extends EntityUsageUpdaterParagraphsTest {

  /**
   * {@inheritdoc}
   */
  protected static string $entityTypeId = 'entity_test';

}
