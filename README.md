# Entity Usage Updater

This module allows you to update references to entities. For example you can update any references to node 21 to point
to node 42 instead. It takes advantage of [the Entity Usage module] which will need to be installed and configured
first.

## Usage

1. [Configure Entity Usage]. If a reference isn't being tracked by Entity Usage then it won't be updated by this module.
2. Go to `admin/config/content/entity-usage-updater` and configure this module.
3. Go to `admin/content/update-references` and enter the details of the target entity you'd like to find and the ID of
   its replacement.

[the Entity Usage module]: https://www.drupal.org/project/entity_usage
[Configure Entity Usage]: https://www.drupal.org/docs/contributed-modules/entity-usage
