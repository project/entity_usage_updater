<?php

namespace Drupal\entity_usage_updater\Event;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Swaps the entity usage tab controller for the one provided by this module.
 */
class EntityUsageUpdaterControllerSubscriber extends RouteSubscriberBase {

  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      $route = $collection->get("entity.$entity_type_id.entity_usage");
      if ($route) {
        $route->setDefault('_controller', '\Drupal\entity_usage_updater\Controller\LocalTaskUsageController::listUsageLocalTask');
      }
    }

    $route = $collection->get('entity_usage.usage_list');
    if ($route) {
      $route->setDefault('_controller', '\Drupal\entity_usage_updater\Controller\LocalTaskUsageController::listUsagePage');
    }
  }

}
