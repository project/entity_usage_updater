<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a base class for entity usage updater plugins.
 */
abstract class EntityUsageUpdaterPluginBase extends PluginBase implements EntityUsageUpdaterPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Checks if two entity objects refer to the same entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $a
   *   The first entity to check.
   * @param \Drupal\Core\Entity\EntityInterface $b
   *   The second entity to check.
   *
   * @return bool
   *   TRUE if the objects refer to the same entity; FALSE otherwise.
   */
  protected function sameEntity(EntityInterface $a, EntityInterface $b): bool {
    return $a->getEntityTypeId() === $b->getEntityTypeId() && $a->id() === $b->id();
  }

}
