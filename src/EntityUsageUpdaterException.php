<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater;

/**
 * Provides an exception class for errors raised during update.
 */
class EntityUsageUpdaterException extends \RuntimeException {}
