<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;

/**
 * Provides an interface for entity usage updater plugins.
 */
interface EntityUsageUpdaterPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Updates entity references within a field item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The target entity to replace references to.
   * @param string $new_entity_type
   *   The new entity type ID.
   * @param string|int $new_id
   *   The new entity ID.
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item to update.
   */
  public function update(EntityInterface $old_target, string $new_entity_type, $new_id, FieldItemInterface $item): void;

  /**
   * Removes entity references within a field item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The target entity to replace references to.
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item to update.
   */
  public function remove(EntityInterface $old_target, FieldItemInterface $item): void;

}
