<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Entity\TranslatableRevisionableInterface;
use Drupal\Core\Entity\TranslatableRevisionableStorageInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\entity_usage\EntityUsageInterface;
use Drupal\entity_usage\EntityUsageTrackInterface;
use Drupal\entity_usage_updater\Utility\ViolationsHelper;

/**
 * Updates references to entities tracked with Entity Usage.
 *
 * @todo Add lock.
 * @todo Support optional entity validation prior to saving.
 * @todo Check that ERR fields aren't translatable (which we assume).
 * @todo check that content moderation isn't enabled for entities being updated?
 * @todo Sanitize exceptions.
 * @todo Improve details in error logging.
 */
class EntityUsageUpdater implements EntityUsageUpdaterInterface {

  /**
   * {@inheritdoc}
   */
  public function update(array $mapping): void {
    $batch_builder = new BatchBuilder();
    $batch_builder
      ->setFinishCallback([static::class, 'finish'])
      ->setTitle(t('Entity reference update'))
      ->setInitMessage(t('Updating entity references.'))
      ->setErrorMessage(t('There was an error updating entity references.'));

    foreach ($mapping as $old_entity_type_id => $entity_updates) {
      foreach ($entity_updates as $old_id => [$new_entity_type_id, $new_id]) {
        $callback = [static::class, 'createBatchOperationsFromUsages'];
        $arguments = [
          $old_entity_type_id,
          $old_id,
          $new_entity_type_id,
          $new_id,
        ];
        $batch_builder->addOperation($callback, $arguments);
      }
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function remove(array $mapping, array $moderation = []): void {
    $batch_builder = new BatchBuilder();
    $batch_builder
      ->setFinishCallback([static::class, 'finish'])
      ->setTitle(t('Entity reference remove'))
      ->setInitMessage(t('Removing entity references.'))
      ->setErrorMessage(t('There was an error removing entity references.'));

    foreach ($mapping as $old_entity_type_id => $entity_updates) {
      foreach ($entity_updates as $old_id) {
        $callback = [static::class, 'createRemoveBatchOperationsFromUsages'];
        $arguments = [
          $old_entity_type_id,
          $old_id,
          $moderation[$old_entity_type_id][$old_id] ?? NULL,
        ];
        $batch_builder->addOperation($callback, $arguments);
      }
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * Finds usages of a particular entity and adds batch update operations.
   *
   * It exists as a standalone batch operation in case the usages of a single
   * entity are particularly large.
   *
   * @param string $old_entity_type_id
   *   The entity type of the entity we're searching for.
   * @param string|int $old_id
   *   The entity ID of the entity we're searching for.
   * @param string $new_entity_type_id
   *   The entity type of the entity to replace it with.
   * @param string|int $new_id
   *   The entity ID of the entity to replace it with.
   * @param array &$context
   *   The batch context.
   */
  public static function createBatchOperationsFromUsages(string $old_entity_type_id, $old_id, string $new_entity_type_id, $new_id, array &$context): void {
    $entity = \Drupal::entityTypeManager()->getStorage($old_entity_type_id)->load($old_id);
    if (!$entity) {
      $context['results']['errors'][] = t("Couldn't load @entity_type @id", [
        '@entity_type_id' => $old_entity_type_id,
        '@id' => $old_id,
      ]);
      return;
    }

    $usages = self::getUsages($entity);
    if (empty($usages)) {
      return;
    }

    $batch_builder = new BatchBuilder();
    $batch_builder
      ->setFinishCallback([static::class, 'finish'])
      ->setTitle(t('Entity reference update'))
      ->setInitMessage(t('Updating entity references.'))
      ->setErrorMessage(t('There was an error updating entity references.'));

    foreach ($usages as $entity_type_id => $entity_type_usages) {
      foreach ($entity_type_usages as $entity_id => $entity_usages) {
        $arguments = [
          $entity,
          $new_entity_type_id,
          $new_id,
          $entity_type_id,
          $entity_id,
          $entity_usages,
        ];
        $batch_builder->addOperation([static::class, 'updateReferences'], $arguments);
      }
    }

    $batch_builder->addOperation([static::class, 'updateRevisionReferenceHostEntities'], [$entity]);
    batch_set($batch_builder->toArray());
  }

  /**
   * Finds usages of a particular entity and adds batch remove operations.
   *
   * It exists as a standalone batch operation in case the usages of a single
   * entity are particularly large.
   *
   * @param string $old_entity_type_id
   *   The entity type of the entity we're searching for.
   * @param string|int $old_id
   *   The entity ID of the entity we're searching for.
   * @param string|null $moderation_state
   *   The publishing state to change the entity to after removing usages.
   *   Supports content moderation states.
   * @param array &$context
   *   The batch context.
   */
  public static function createRemoveBatchOperationsFromUsages(string $old_entity_type_id, $old_id, string|null $moderation_state, array &$context): void {
    // It is possible to configure a redirect loop.
    static $seen_redirects = [];

    $entity = \Drupal::entityTypeManager()->getStorage($old_entity_type_id)->load($old_id);
    if (!$entity) {
      $context['results']['errors'][] = t("Couldn't load @entity_type @id", [
        '@entity_type_id' => $old_entity_type_id,
        '@id' => $old_id,
      ]);
      return;
    }

    $usages = self::getUsages($entity);
    if (empty($usages) && $moderation_state === NULL) {
      // There is nothing to do.
      return;
    }

    $batch_builder = new BatchBuilder();
    $batch_builder
      ->setFinishCallback([static::class, 'finish'])
      ->setTitle(t('Entity reference update'))
      ->setInitMessage(t('Removing entity references.'))
      ->setErrorMessage(t('There was an error removing entity references.'));

    // Redirect integration.
    if (isset($usages['redirect'])) {
      foreach ($usages['redirect'] as $entity_id => $entity_usages) {
        if (isset($seen_redirects[$entity_id])) {
          continue;
        }
        $seen_redirects[$entity_id] = TRUE;
        $callback = [static::class, 'createRemoveBatchOperationsFromUsages'];
        $arguments = [
          'redirect',
          $entity_id,
          NULL,
        ];
        $batch_builder->addOperation($callback, $arguments);
      }
    }

    foreach ($usages as $entity_type_id => $entity_type_usages) {
      // Redirects do not have a field UI and the only trackable field on it is
      // a link base field that is required so we cannot remove the value
      // without creating an validation error. If the redirect points to an
      // entity you are removing links to and unpublishing then the resulting
      // redirect will still work and redirect people to the now unpublished
      // entity in the same way if they had the direct link to content.
      if ($entity_type_id === 'redirect') {
        continue;
      }
      foreach ($entity_type_usages as $entity_id => $entity_usages) {
        $arguments = [
          $entity,
          $entity_type_id,
          $entity_id,
          $entity_usages,
        ];
        $batch_builder->addOperation([
          static::class,
          'removeReferences',
        ], $arguments);
      }
    }

    if (!empty($usages)) {
      $batch_builder->addOperation([
        static::class,
        'updateRevisionReferenceHostEntities',
      ], [$entity]);
    }

    if ($moderation_state !== NULL) {
      $batch_builder->addOperation([static::class, 'moderateEntity'], [$old_entity_type_id, $old_id, $moderation_state]);
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * Updates references within a single entity.
   *
   * It handles all translations for the given entity in one go.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The entity that should no longer be referenced.
   * @param string $new_target_entity_type
   *   The entity type of the entity that should be referenced instead of
   *   $old_target.
   * @param int|string $new_target_id
   *   The entity ID of the entity that should be referenced instead of
   *   $old_target.
   * @param string $entity_type_id
   *   The entity type of the entity being updated.
   * @param int|string $entity_id
   *   The ID of the entity being updated.
   * @param string[][] $usages
   *   An array of associative arrays of usage information, keyed by:
   *   - source_langcode
   *   - method
   *   - field_name
   *   - count.
   * @param array &$context
   *   The batch API context array.
   */
  public static function updateReferences(EntityInterface $old_target, string $new_target_entity_type, $new_target_id, string $entity_type_id, $entity_id, array $usages, array &$context) {
    $log_message = (string) t("Automatically updated by the entity usage updater module to replace @old_entity_type @old_entity_id with @new_entity_type @new_entity_id", [
      '@old_entity_type' => $old_target->getEntityTypeId(),
      '@old_entity_id' => $old_target->id(),
      '@new_entity_type' => $new_target_entity_type,
      '@new_entity_id' => $new_target_id,
    ]);
    $func = function ($updater, $item) use ($old_target, $new_target_entity_type, $new_target_id) {
      $updater->update($old_target, $new_target_entity_type, $new_target_id, $item);
    };
    self::processReferences($old_target, $entity_type_id, $entity_id, $func, $log_message, $usages, $context);
  }

  /**
   * Removes references within a single entity.
   *
   * It handles all translations for the given entity in one go.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The entity that should no longer be referenced.
   * @param string $entity_type_id
   *   The entity type of the entity being updated.
   * @param int|string $entity_id
   *   The ID of the entity being updated.
   * @param string[][] $usages
   *   An array of associative arrays of usage information, keyed by:
   *   - source_langcode
   *   - method
   *   - field_name
   *   - count.
   * @param array &$context
   *   The batch API context array.
   */
  public static function removeReferences(EntityInterface $old_target, string $entity_type_id, $entity_id, array $usages, array &$context): void {
    $func = function (EntityUsageUpdaterPluginInterface $updater, FieldItemInterface $item) use ($old_target) {
      $updater->remove($old_target, $item);
    };
    $log_message = (string) t("Automatically updated by the entity usage updater module to remove @old_entity_type @old_entity_id", [
      '@old_entity_type' => $old_target->getEntityTypeId(),
      '@old_entity_id' => $old_target->id(),
    ]);
    $context['sandbox']['filter_empty_items'] = TRUE;
    self::processReferences($old_target, $entity_type_id, $entity_id, $func, $log_message, $usages, $context);
  }

  /**
   * Processes references within a single entity.
   *
   * It handles all translations for the given entity in one go.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The entity that should no longer be referenced.
   * @param string $entity_type_id
   *   The entity type of the entity being updated.
   * @param int|string $entity_id
   *   The ID of the entity being updated.
   * @param callable $func
   *   A function that accepts an EntityUsageUpdaterPluginInterface and a field
   *   item to update.
   * @param string $log_message
   *   The log message to use when the entity is saved.
   * @param string[][] $usages
   *   An array of associative arrays of usage information, keyed by:
   *   - source_langcode
   *   - method
   *   - field_name
   *   - count.
   * @param array &$context
   *   The batch API context array.
   */
  private static function processReferences(EntityInterface $old_target, string $entity_type_id, $entity_id, callable $func, string $log_message, array $usages, array &$context): void {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $updater_manager = \Drupal::service('plugin.manager.entity_usage_updater');
    assert($updater_manager instanceof PluginManagerInterface);
    $tracker_manager = \Drupal::service('plugin.manager.entity_usage.track');
    assert($tracker_manager instanceof PluginManagerInterface);

    // I'm not sure if this is the best way to support all possible multilingual
    // pending revision scenarios. We just load the latest revision, and then
    // update all the translations which need updating.
    if ($entity_type->isRevisionable()) {
      assert($storage instanceof RevisionableStorageInterface);
      $vid = $storage->getLatestRevisionId($entity_id);
      $current_revision = $storage->loadRevision($vid);
    }
    else {
      $current_revision = $storage->load($entity_id);
    }

    // Determine if the entity is currently valid.
    assert($current_revision instanceof FieldableEntityInterface);
    $violations = $current_revision->validate();
    $initial_entity_violations = $violations->count();

    if ($entity_type->isRevisionable()) {
      $new_revision = static::createRevision($current_revision);
    }
    else {
      $new_revision = $current_revision;
    }

    foreach ($usages as $reference) {
      $langcode = $reference['source_langcode'];

      if ($current_revision instanceof TranslatableInterface) {
        assert($new_revision instanceof TranslatableInterface);
        $current_translation = $current_revision->getTranslation($langcode);
        $new_translation = $new_revision->getTranslation($langcode);
      }
      else {
        $current_translation = $current_revision;
        $new_translation = $new_revision;
      }
      assert($new_translation instanceof FieldableEntityInterface);

      static::processRevisionReferences($current_translation, $context);
      assert(!$new_translation instanceof RevisionableInterface || $new_translation->isLatestRevision());
      assert($new_translation->language()->getId() === $reference['source_langcode']);

      $method = $reference['method'];
      try {
        // Use the Entity Usage tracker to help us identify which field deltas
        // contain references to the target entity. (That level of detail isn't
        // provided in the usage information.)
        $tracker = $tracker_manager->createInstance($method);
        assert($tracker instanceof EntityUsageTrackInterface);
        $updater = $updater_manager->createInstance($method);
        assert($updater instanceof EntityUsageUpdaterPluginInterface);

        $field = $new_translation->get($reference['field_name']);
        assert($field instanceof FieldItemListInterface);
        foreach ($field as $item) {
          // Use entity_usage to verify if the target entity is in the field.
          $old_target_string = $old_target->getEntityTypeId() . '|' . $old_target->id();
          if (in_array($old_target_string, $tracker->getTargetEntities($item), TRUE)) {
            $func($updater, $item);
          }
        }
        if ($context['sandbox']['filter_empty_items'] ?? FALSE) {
          $field->filterEmptyItems();
        }
      }
      catch (PluginException $e) {
        $context['results']['errors'][] = $e->getMessage();
      }
    }

    if ($new_revision instanceof RevisionLogInterface) {
      $new_revision->setRevisionLogMessage($log_message);
    }

    // Ensure we are not making increasing the number of violations.
    assert($new_revision instanceof FieldableEntityInterface);
    $violations = $new_revision->validate();
    if ($violations->count() > $initial_entity_violations) {
      $context['results']['errors'][] = ViolationsHelper::toTranslatableMarkup($violations);
      \Drupal::logger('entity_usage_updater')->error(...ViolationsHelper::toLogMessage($violations));
      return;
    }

    $new_revision->save();

    if (!isset($context['results']['update_count'])) {
      $context['results']['update_count'] = 0;
      $context['results']['entity_id'] = $old_target->id();
      $context['results']['entity_type'] = $old_target->getEntityTypeId();
    }
    $context['results']['update_count']++;
  }

  /**
   * Tracks entities referenced via entity reference revision fields.
   *
   * If you update a field on an entity that's itself referenced via an ERR
   * field, then you also need to update the ERR field as otherwise it will
   * still refer to the earlier revision ID prior to the update. For example if
   * you update a link field on a paragraph, you need to make sure that any ERR
   * field referencing that paragraph is updated to reference the new paragraph
   * revision ID. This can be recursive: if you have nested paragraphs, you have
   * to keep updating references to the updated paragraph.
   *
   * For now this is dumb, and just checks if the entity is a paragraph. Other
   * entity types using ERR fields will not be updated. (But it's ok if there's
   * a non-ERR field referencing a paragraph, it will be tracked but won't cause
   * any problems.)
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being updated.
   * @param array $context
   *   The batch context array.
   */
  protected static function processRevisionReferences(EntityInterface $entity, array &$context): void {
    // @todo More generally we want to consider any entity that's referenced by
    //   an ERR field. However I don't know if I can make the same assumptions
    //   about i18n with ERR fields in general that I can with paragraphs
    //   (because the paragraphs instructions are very clear).
    if ($entity->getEntityTypeId() === 'paragraph') {
      assert($entity instanceof TranslatableRevisionableInterface);
      // If we assume everything works like paragraphs wrt i18n then the
      // language is irrelevant. The ERR field is not translatable, and its
      // one value should be updated to the latest paragraph vid. That in turn
      // should have all the correct values for the various languages already.
      $context['results']['updated_err_entities'][] = [
        'id' => $entity->id(),
        'vid' => $entity->getRevisionId(),
        'entity_type' => $entity->getEntityTypeId(),
      ];
    }
  }

  /**
   * Updates entity reference revision fields referencing updated entities.
   *
   * If you just update a paragraph that references an entity, then you won't
   * see any changes on the host entity; this is because entity reference
   * revision fields reference revisions, so the field is still explicitly
   * referencing the old revision. This function will find such "stale"
   * revisions and update the host entities to reference the latest revision.
   *
   * It can add to its own workload: for example if there are nested paragraphs
   * then when it updates the inner paragraph, it will update the context array
   * so that it will later update the outer one. The operation should not
   * complete until all such references have been dealt with. (So it's unknown
   * at the start of the operation how many iterations it will take.)
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The entity that should no longer be referenced.
   * @param array $context
   *   The batch API context array.
   */
  public static function updateRevisionReferenceHostEntities(EntityInterface $old_target, array &$context): void {
    // Use sandbox for communication between iterations of the same operation.
    if (empty($context['sandbox']['host_entities']) && !empty($context['results']['updated_err_entities'])) {
      // It's possible this will cause a DB max packet error, depending on how
      // many entities are passed: it would be good to batch it as well. It's
      // also plausible that getHostEntities will exhaust memory, but it's not
      // loading entities, just querying for IDs and returning them, so less
      // likely to be an issue.
      $context['sandbox']['host_entities'] = static::getHostEntities($context['results']['updated_err_entities']);
      $context['results']['updated_err_entities'] = [];
    }

    // Return if we've already handled all entities with updated ERR field
    // targets.
    if (empty($context['sandbox']['host_entities'])) {
      return;
    }

    $batch_size = Settings::get('entity_usage_updater_batch_size') ?? Settings::get('entity_update_batch_size', 50);

    $entity_type_id = key($context['sandbox']['host_entities']);
    $batch = array_slice($context['sandbox']['host_entities'][$entity_type_id], 0, $batch_size, TRUE);
    array_splice($context['sandbox']['host_entities'][$entity_type_id], 0, $batch_size);
    if (empty($context['sandbox']['host_entities'][$entity_type_id])) {
      unset($context['sandbox']['host_entities'][$entity_type_id]);
    }

    $ids = array_keys($batch);
    $repository = \Drupal::service('entity.repository');
    assert($repository instanceof EntityRepositoryInterface);

    foreach ($repository->getActiveMultiple($entity_type_id, $ids) as $entity) {
      assert($entity instanceof FieldableEntityInterface);
      // In case of nested ERR fields (eg. nested paragraphs) we still need to
      // track as we update.
      static::processRevisionReferences($entity, $context);

      $revision = static::createRevision($entity);
      foreach ($batch[$revision->id()] as $field_name => $targets) {
        // We're just updating the revision to the latest active.
        $field = $revision->{$field_name};
        assert($field instanceof FieldItemListInterface);
        $langcode = $field->getLangcode();
        $target_type = $field->getFieldDefinition()->getSetting('target_type');
        // Find which field deltas need updating.
        foreach ($field as $value) {
          assert($value instanceof EntityReferenceRevisionsItem);
          // If the field items target id is in the list of targets then there
          // is a new revision to reference.
          if (isset($targets[$value->target_id])) {
            $value->target_revision_id = static::getActiveRevisionId($target_type, $value->target_id, $langcode);
            // By default, when adding a new revision the Entity Reference
            // Revisions module will create a new paragraph entity revision.
            // However, in this case we have already created the revision.
            // Therefore, this module overrides
            // \Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem
            // to add a flag to prevent this.
            $value->entity->entity_usage_updater_prevent_paragraph_save = TRUE;
          }
        }
      }

      // @todo A better log message might be nice, but it will involve storing
      //   more data than we currently do.
      if ($revision instanceof RevisionLogInterface) {
        $revision->setRevisionLogMessage((string) t("Automatically updated by the entity usage updater module."));
      }
      $revision->save();

      if (!isset($context['results']['update_count'])) {
        $context['results']['update_count'] = 0;
        $context['results']['entity_id'] = $old_target->id();
        $context['results']['entity_type'] = $old_target->getEntityTypeId();
      }
      $context['results']['update_count']++;
    }

    $context['finished'] = 0;
  }

  /**
   * Queries for referencing host entities using ERR fields.
   *
   * It takes details of entities that have been updated and systematically goes
   * through all entity reference revision fields to see if any of them
   * reference updated entities, and returns the details of those that do. It
   * doesn't use the Entity Usage module because that might not be tracking
   * the host entities and it doesn't accept a revision ID to check for.
   *
   * @param array $entity_ids
   *   An array of associative arrays of now stale entity references, keyed by
   *   - id
   *   - vid
   *   - entity_type.
   *
   * @return array
   *   An array containing details of entities with ERR fields that now point to
   *   stale revisions and so need to be updated to the latest revision, eg.
   *   @code
   *   [
   *     'node' => [
   *       // 42 is *revision* ID
   *       42 => [
   *         // The ERR field that needs updating.
   *         'field_a' => [
   *           // Which ERR entities need updating.
   *           1 => TRUE,
   *           4 => TRUE,
   *         ],
   *         'field_b' => [
   *           3 => TRUE,
   *         ],
   *       ],
   *     ],
   *   ]
   *   @endcode
   */
  public static function getHostEntities(array $entity_ids): array {
    if (!$entity_ids) {
      return [];
    }
    $entity_field_manager = \Drupal::service('entity_field.manager');
    assert($entity_field_manager instanceof EntityFieldManagerInterface);
    $map = $entity_field_manager->getFieldMapByFieldType('entity_reference_revisions');
    $out = [];
    foreach ($map as $host_entity_type_id => $field_map) {
      $storage = \Drupal::entityTypeManager()->getStorage($host_entity_type_id);
      $is_revisionable = $storage->getEntityType()->isRevisionable();
      $field_storage_definitions = $entity_field_manager->getFieldStorageDefinitions($host_entity_type_id);
      foreach ($field_map as $field_name => $field_data) {
        // @todo If https://www.drupal.org/project/drupal/issues/3394248 is
        //   resolved the multiple queries inside the following loop can be
        //   changed to a single query using OR and AND conditions. This would
        //   be a revert of https://www.drupal.org/i/3494480.
        foreach ($entity_ids as ['id' => $entity_id, 'vid' => $revision_id, 'entity_type' => $entity_type_id]) {
          if ($field_storage_definitions[$field_name]->getSetting('target_type') === $entity_type_id) {
            // As ERR fields shouldn't be marked as translatable, we don't set the
            // language (which won't work with content moderation until #3088341
            // lands anyway).
            $query = $storage->getQuery()
              ->accessCheck(FALSE);
            if ($is_revisionable) {
              $query->allRevisions();
            }
            $query->condition("$field_name.target_id", $entity_id);
            $query->condition("$field_name.target_revision_id", $revision_id);
            // Ignore revision ID, just assume it's the active ID for the entity.
            // (In case the entity gets updated in the meantime by this script.)
            foreach ($query->execute() as $id) {
              $out[$host_entity_type_id][$id][$field_name][$entity_id] = TRUE;
            }
          }
        }
      }
    }
    return $out;
  }

  /**
   * Returns the active revision ID for a given entity.
   *
   * The active revision is appropriate for editing, though it might not be the
   * currently visible one. I'm not sure this plays nicely with all possible
   * forms of multilingual pending revisions.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param int|string $entity_id
   *   The entity ID.
   * @param string $langcode
   *   The language code.
   *
   * @return int
   *   The revision ID.
   *
   * @throws \RuntimeException
   *   If no revisions are found.
   */
  protected static function getActiveRevisionId(string $entity_type_id, $entity_id, string $langcode): int {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    // Latest revision IDs are cached by ContentEntityStorageBase.
    assert($storage instanceof RevisionableStorageInterface);
    if ($storage instanceof TranslatableRevisionableStorageInterface) {
      $active_vid = $storage->getLatestTranslationAffectedRevisionId($entity_id, $langcode);
    }
    else {
      $active_vid = $storage->getLatestRevisionId($entity_id);
    }
    if ($active_vid === NULL) {
      throw new \RuntimeException("No revision found for $entity_type_id $entity_id $langcode");
    }
    return intval($active_vid);
  }

  /**
   * Creates a new revision from an entity/revision if possible.
   *
   * If a new revision is created it will be default if $entity was default.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create a new revision of.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   For revisionable entities a new revision; otherwise $entity.
   */
  protected static function createRevision(EntityInterface $entity): EntityInterface {
    $storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());
    if ($storage->getEntityType()->isRevisionable()) {
      assert($entity instanceof RevisionableInterface);
      assert($storage instanceof RevisionableStorageInterface);
      return $storage->createRevision($entity, $entity->isDefaultRevision());
    }
    return $entity;
  }

  /**
   * Displays messages after finishing with a batch.
   *
   * It's fairly generic and used for different batches.
   *
   * @param bool $success
   *   Indicate that the batch API tasks were all completed successfully.
   * @param array $results
   *   An array of all the results that were updated in update_do_one().
   * @param array $operations
   *   A list of the operations that had not been completed by the batch API.
   */
  public static function finish($success, $results, $operations) {
    if (!$success) {
      \Drupal::messenger()->addError("The update didn't complete successfully.");
    }

    $map = [
      'errors' => MessengerInterface::TYPE_ERROR,
      'warnings' => MessengerInterface::TYPE_WARNING,
      'messages' => MessengerInterface::TYPE_STATUS,
    ];
    foreach ($map as $key => $type) {
      if (!empty($results[$key])) {
        foreach ($results[$key] as $message) {
          \Drupal::messenger()->addMessage($message, $type);
        }
      }
    }

    if ($success && isset($results['update_count'])) {
      $for_entity = \Drupal::entityTypeManager()->getStorage($results['entity_type'])->load($results['entity_id']);
      $message = \Drupal::translation()->formatPlural(
        $results['update_count'],
        "Updated one entity for references to %label.",
        "Updated @count entities for references to %label.",
        ['%label' => $for_entity?->label() ?? 'Unknown']
      );
      \Drupal::messenger()->addStatus($message);
    }
    if ($success && isset($results['moderation_message'])) {
      \Drupal::messenger()->addStatus($results['moderation_message']);
    }
  }

  /**
   * Gets the list of usages for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get usages of.
   *
   * @return array<string, array<string, array<array{source_langcode: string, method: string, field_name: string, count: string}>>>
   *   See \Drupal\entity_usage\EntityUsageInterface::listSources().
   *
   * @todo Move this to a service.
   */
  public static function getUsages(EntityInterface $entity): array {
    $entity_usage = \Drupal::service('entity_usage.usage');
    assert($entity_usage instanceof EntityUsageInterface);
    $usages = $entity_usage->listSources($entity);
    foreach ($usages as $entity_type_id => $entity_type_usages) {
      // For revisionable entity types, we're only interested in references that
      // are in the active revision.
      if (\Drupal::entityTypeManager()->getDefinition($entity_type_id)->isRevisionable()) {
        foreach ($entity_type_usages as $entity_id => $entity_usages) {
          $is_active = fn(array $usage): bool => static::getActiveRevisionId($entity_type_id, $entity_id, $usage['source_langcode']) === (int) $usage['source_vid'];
          $usages[$entity_type_id][$entity_id] = array_values(array_filter($entity_usages, $is_active));
          // Unset the source VID - we've already verified it's the latest
          // revision. It's possible the recorded source VID might no longer be
          // the latest one when we come to update the usage, so keeping it can
          // be actively misleading - just always update the latest revision in
          // the appropriate language.
          foreach ($usages[$entity_type_id][$entity_id] as $key => $batch_item) {
            unset($usages[$entity_type_id][$entity_id][$key]['source_vid']);
          }
        }
      }
      // Remove any empty arrays (ie entities that only had references in old
      // revisions).
      $usages[$entity_type_id] = array_filter($usages[$entity_type_id]);
    }
    // Remove any empty array (ie. entity types that are now empty).
    return array_filter($usages);
  }

  /**
   * Moderates an entity.
   *
   * @param string $entity_type_id
   *   The entity type of the entity to be moderated.
   * @param string|int $entity_id
   *   The entity ID of the entity to be moderated.
   * @param string $moderation_state
   *   The state to change the entity to.
   * @param array $context
   *   The batch context.
   *
   * @return void
   */
  public static function moderateEntity(string $entity_type_id, string|int $entity_id, string $moderation_state, array &$context): void {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    if (!$entity) {
      $context['results']['errors'][] = t("Couldn't load @entity_type @id", [
        '@entity_type_id' => $entity_type_id,
        '@id' => $entity_id,
      ]);
      return;
    }

    if (!$entity->access('update')) {
      \Drupal::messenger()->addStatus(t('Denied access to unpublish %label.', ['%label' => $entity->label() ?? 'Unknown']));
      return;
    }

    /** @var ?\Drupal\content_moderation\ModerationInformationInterface $moderation_info */
    $moderation_info = \Drupal::hasService('content_moderation.moderation_information') ? \Drupal::service('content_moderation.moderation_information') : NULL;
    if (!$moderation_info?->isModeratedEntity($entity)) {
      if (!$entity instanceof EntityPublishedInterface) {
        return;
      }

      $current_state = $entity->isPublished() ? 'published' : 'unpublished';
      if ($current_state === $moderation_state) {
        return;
      }

      $revision = static::createRevision($entity);
      assert($revision instanceof EntityPublishedInterface);
      $moderation_state === 'published' ? $revision->setPublished() : $revision->setUnpublished();
      $moderation_state_label = $moderation_state === 'published' ? t('Published') : t('Unpublished');
    }
    else {
      $current_state = $entity->moderation_state->value;
      if ($current_state === $moderation_state) {
        return;
      }
      $revision = static::createRevision($entity);
      assert($revision instanceof ContentEntityInterface);
      $revision->set('moderation_state', $moderation_state);
      $workflow = $moderation_info->getWorkflowForEntity($revision);
      $moderation_state_label = $workflow->getTypePlugin()->getState($moderation_state)->label();
    }

    if ($revision instanceof RevisionLogInterface) {
      $revision->setRevisionLogMessage('Entity usage updater moderation status change');
    }
    // The entity is not validated here because we are only changing the
    // moderation state which is not the same as changing field values.
    $revision->save();
    $context['results']['moderation_message'] = t("Updated moderation state for %label to %state.", ['%label' => $revision->label() ?: $revision->id(), '%state' => $moderation_state_label]);
  }

}
