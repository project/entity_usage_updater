<?php

namespace Drupal\entity_usage_updater\Controller;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_usage\Controller\LocalTaskUsageController as BaseLocalTaskUsageController;
use Drupal\entity_usage_updater\Form\EntityUsageUpdateForm;

/**
 * Controller to add form to entity usage local tasks.
 */
class LocalTaskUsageController extends BaseLocalTaskUsageController {

  /**
   * {@inheritdoc}
   */
  public function listUsagePage($entity_type, $entity_id): array {
    $build = parent::listUsagePage($entity_type, $entity_id);
    // Only add the form if there are usages.
    if (array_keys($build) !== ['#markup']) {
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
      $form = $this->formBuilder()->getForm(EntityUsageUpdateForm::class, $entity);
      // @todo add ability to move the form?
      $build['entity_usage_updater'] = [
        '#type' => 'fieldset',
        '#title' => t('Update usages'),
        '#tree' => FALSE,
        '#access' => $form['#access'],
      ];
      $build['entity_usage_updater']['form'] = $form;
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function listUsageLocalTask(RouteMatchInterface $route_match): array {
    $entity = $this->getEntityFromRouteMatch($route_match);
    return $this->listUsagePage($entity->getEntityTypeId(), $entity->id());
  }

}
