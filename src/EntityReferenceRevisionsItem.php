<?php

namespace Drupal\entity_usage_updater;

use Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem as ModuleEntityReferenceRevisionsItem;

/**
 * Overrides ERR class to prevent unnecessary revisions being created.
 */
class EntityReferenceRevisionsItem extends ModuleEntityReferenceRevisionsItem {

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    if ($this->entity && $this->entity->entity_usage_updater_prevent_paragraph_save) {
      return;
    }
    parent::preSave();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    if ($this->entity && $this->entity->entity_usage_updater_prevent_paragraph_save) {
      unset($this->entity->entity_usage_updater_prevent_paragraph_save);
    }
    return parent::postSave($update);
  }

}
