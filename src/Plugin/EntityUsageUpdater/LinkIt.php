<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Plugin\EntityUsageUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\entity_usage\UrlToEntityInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterPluginBase;
use Drupal\entity_usage_updater\LinkRemoverTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a plugin for updating LinkIt links within text fields.
 *
 * @EntityUsageUpdater(
 *   id = "linkit",
 *   label = @Translation("Linkit"),
 *   description = @Translation("Updates LinkIt links to entities within text fields.")
 * )
 */
class LinkIt extends EntityUsageUpdaterPluginBase implements ContainerFactoryPluginInterface {
  use LinkRemoverTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The URL helper object.
   *
   * @var \Drupal\entity_usage\UrlToEntityInterface
   */
  protected UrlToEntityInterface $urlToEntity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static($configuration, $plugin_id, $plugin_definition);
    $plugin->entityTypeManager = $container->get('entity_type.manager');
    $plugin->urlToEntity = $container->get(UrlToEntityInterface::class);
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $old_target, string $new_entity_type, $new_id, $item): void {
    $this->updateItemProperty($old_target, $new_entity_type, $new_id, $item->get('value'));
    if (isset($item->summary)) {
      $this->updateItemProperty($old_target, $new_entity_type, $new_id, $item->get('summary'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function remove(EntityInterface $old_target, FieldItemInterface $item): void {
    $value = $item->get('value');
    assert($value instanceof StringInterface);
    $this->removeLinks($old_target, $value, $this->urlToEntity, $this->entityTypeManager);
    if (isset($item->summary)) {
      $summary = $item->get('summary');
      assert($summary instanceof StringInterface);
      $this->removeLinks($old_target, $summary, $this->urlToEntity, $this->entityTypeManager);
    }
  }

  /**
   * Updates LinkIt links to entities within a string property.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The target entity to replace references to.
   * @param string $new_entity_type
   *   The new entity type ID.
   * @param string|int $new_id
   *   The new entity ID.
   * @param \Drupal\Core\TypedData\Type\StringInterface $text
   *   The string item to search within.
   */
  protected function updateItemProperty(EntityInterface $old_target, string $new_entity_type, $new_id, StringInterface $text): void {
    if (!trim($text->getValue())) {
      return;
    }
    $delimiter = '~';
    $get_attribute_regex = fn (string $attribute, string $value, string $group): string =>
      '\b' . $attribute . '\s*=\s*(?P<quote_' . $group . '>[\'"])' . preg_quote($value, $delimiter) . '(?P=quote_' . $group . ')';
    $entity_type_regex = $get_attribute_regex('data-entity-type', $old_target->getEntityTypeId(), '1');
    $entity_uuid_regex = $get_attribute_regex('data-entity-uuid', $old_target->uuid(), '2');

    $searches = [
      $delimiter . $entity_type_regex . '\s+' . $entity_uuid_regex . $delimiter . 'i',
      $delimiter . $entity_uuid_regex . '\s+' . $entity_type_regex . $delimiter . 'i',
    ];

    $new_entity = $this->entityTypeManager->getStorage($new_entity_type)
      ->load($new_id);
    if (!$new_entity) {
      throw new \RuntimeException("Couldn't load $new_entity_type $new_id");
    }

    $replacement = 'data-entity-type="' . $new_entity_type . '" data-entity-uuid="' . $new_entity->uuid() . '"';
    $text->setValue(preg_replace($searches, $replacement, $text->getValue()));
  }

}
