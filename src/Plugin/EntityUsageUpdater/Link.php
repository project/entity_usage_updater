<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Plugin\EntityUsageUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterPluginBase;

/**
 * Provides a plugin for updating link fields.
 *
 * @EntityUsageUpdater(
 *   id = "link",
 *   label = @Translation("Link"),
 *   description = @Translation("Updates link fields referencing entities.")
 * )
 */
class Link extends EntityUsageUpdaterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $old_target, string $new_entity_type, $new_id, FieldItemInterface $item): void {
    $item->set('uri', "entity:$new_entity_type/$new_id");
  }

  /**
   * {@inheritdoc}
   */
  public function remove(EntityInterface $old_target, FieldItemInterface $item): void {
    $item->set('uri', NULL);
  }

}
