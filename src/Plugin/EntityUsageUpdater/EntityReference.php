<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Plugin\EntityUsageUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterPluginBase;

/**
 * Provides a plugin for updating entity reference fields.
 *
 * @EntityUsageUpdater(
 *   id = "entity_reference",
 *   label = @Translation("Entity Reference"),
 *   description = @Translation("Updates entity reference fields."),
 * )
 */
class EntityReference extends EntityUsageUpdaterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $old_target, string $new_entity_type, $new_id, FieldItemInterface $item): void {
    assert($item->getParent() instanceof EntityReferenceFieldItemListInterface);
    $item->set('target_id', $new_id);
  }

  /**
   * {@inheritdoc}
   */
  public function remove(EntityInterface $old_target, FieldItemInterface $item): void {
    assert($item->getParent() instanceof EntityReferenceFieldItemListInterface);
    $item->set('target_id', NULL);
  }

}
