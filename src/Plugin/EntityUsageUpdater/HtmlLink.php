<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Plugin\EntityUsageUpdater;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\Core\Url;
use Drupal\entity_usage\UrlToEntityInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterException;
use Drupal\entity_usage_updater\EntityUsageUpdaterPluginBase;
use Drupal\entity_usage_updater\LinkRemoverTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a plugin for updating links within text fields.
 *
 * @todo Support converting to linkit style links.
 *
 * @EntityUsageUpdater(
 *   id = "html_link",
 *   label = @Translation("HTML Link"),
 *   description = @Translation("Updates links to entities within text fields.")
 * )
 */
class HtmlLink extends EntityUsageUpdaterPluginBase implements ContainerFactoryPluginInterface, PluginFormInterface {
  use LinkRemoverTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The language negotiator service.
   *
   * @var \Drupal\language\LanguageNegotiatorInterface
   */
  protected $languageNegotiator;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The URL helper object.
   *
   * @var \Drupal\entity_usage\UrlToEntityInterface
   */
  protected UrlToEntityInterface $urlToEntity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static($configuration, $plugin_id, $plugin_definition);
    $plugin->entityTypeManager = $container->get('entity_type.manager');
    $plugin->languageManager = $container->get('language_manager', ContainerInterface::NULL_ON_INVALID_REFERENCE);
    $plugin->languageNegotiator = $container->get('language_negotiator', ContainerInterface::NULL_ON_INVALID_REFERENCE);
    $plugin->logger = $container->get('logger.channel.entity_usage_updater');
    $plugin->urlToEntity = $container->get(UrlToEntityInterface::class);
    $plugin->setStringTranslation($container->get('string_translation'));
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['convert_to_linkit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Convert normal links to LinkIt links."),
      '#default_value' => $this->configuration['convert_to_linkit'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $old_target, string $new_entity_type, $new_id, FieldItemInterface $item): void {
    try {
      $value = $item->get('value');
      assert($value instanceof StringInterface);
      $this->updateItemProperty($old_target, $new_entity_type, $new_id, $value);
      if (isset($item->summary)) {
        $summary = $item->get('summary');
        assert($summary instanceof StringInterface);
        $this->updateItemProperty($old_target, $new_entity_type, $new_id, $summary);
      }
    }
    catch (EntityUsageUpdaterException) {
      $host = $item->getEntity();
      $context = [
        '@host_entity_type' => $host->getEntityType()->getLabel(),
        '@host_id' => $host->id(),
        '@field_name' => $item->getFieldDefinition()->getLabel(),
        '@target_entity_type' => $old_target->getEntityType()->getLabel(),
        '@target_id' => $old_target->id(),
      ];
      $this->logger->warning("Unable to update reference to @target_entity_type with ID @target_id in text field @field_name on @host_entity_type with ID @host_id because it doesn't have a canonical link template.", $context);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function remove(EntityInterface $old_target, FieldItemInterface $item): void {
    $value = $item->get('value');
    assert($value instanceof StringInterface);
    $this->removeLinks($old_target, $value, $this->urlToEntity, $this->entityTypeManager);
    if (isset($item->summary)) {
      $summary = $item->get('summary');
      assert($summary instanceof StringInterface);
      $this->removeLinks($old_target, $summary, $this->urlToEntity, $this->entityTypeManager);
    }
  }

  /**
   * Updates links to entities within a string property.
   *
   * @param \Drupal\Core\Entity\EntityInterface $old_target
   *   The target entity to replace references to.
   * @param string $new_entity_type
   *   The new entity type ID.
   * @param string|int $new_id
   *   The new entity ID.
   * @param \Drupal\Core\TypedData\Type\StringInterface $text
   *   The string item to search within.
   *
   * @throws \Drupal\entity_usage_updater\EntityUsageUpdaterException
   *   If the target entity is found but doesn't have a canonical link.
   */
  protected function updateItemProperty(EntityInterface $old_target, string $new_entity_type, $new_id, StringInterface $text): void {
    if (!trim($text->getValue())) {
      return;
    }
    $hrefs = [];
    $parsed_entities = $this->parseEntitiesFromText($text->getValue());
    foreach ($parsed_entities as [$entity, $element]) {
      assert($entity instanceof EntityInterface);
      assert($element instanceof \DOMElement);
      if ($this->sameEntity($old_target, $entity)) {
        if ($this->canCreateEntityLinks($new_entity_type)) {
          $href = $element->getAttribute('href');
          $language = $this->getLanguage($href);
          $hrefs[$language][] = $href;
        }
        else {
          throw new EntityUsageUpdaterException("Can't create entity links for new entity type.");
        }
      }
    }
    foreach ($hrefs as $language => $language_hrefs) {
      foreach ($language_hrefs as $href) {
        $parts = UrlHelper::parse($href);
        $options = [
          'fragment' => $parts['fragment'],
          'query' => $parts['query'],
          'absolute' => UrlHelper::isExternal($href),
        ];

        if ($this->configuration['convert_to_linkit'] ?? FALSE) {
          $pattern = '~<a\s+[^>]*href\s*=\s*([\'"])' . preg_quote($href, '~') . '\1(\s+[^>]*)?>~i';
          $new_href = Url::fromRoute("entity.$new_entity_type.canonical", [$new_entity_type => $new_id], $options)->toString();
          $uuid = $this->getUuid($new_entity_type, $new_id);
          $replacement = '<a data-entity-substitution="canonical" data-entity-type="' . $new_entity_type . '" data-entity-uuid="' . $uuid . '" href="' . $new_href . '">';
          $text->setValue(preg_replace($pattern, $replacement, $text->getValue()));
        }
        else {
          if ($this->languageManager) {
            $options['language'] = $this->languageManager->getLanguage($language);
          }
          $replacement_url = Url::fromRoute("entity.$new_entity_type.canonical", [$new_entity_type => $new_id], $options);
          $replacement = 'href="' . $replacement_url->toString() . '"';
          $delimiter = '~';
          $pattern = $delimiter . '\bhref\s*=\s*([\'"])' . preg_quote($href, $delimiter) . '\1' . $delimiter;
          $text->setValue(preg_replace($pattern, $replacement, $text->getValue()));
        }
      }
    }
  }

  /**
   * Extracts entity links from the text.
   *
   * @param string $text
   *   The text containing links.
   *
   * @see \Drupal\entity_usage\Plugin\EntityUsage\Track\HtmlLink::parseEntitiesFromText()
   */
  protected function parseEntitiesFromText(string $text) {
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);
    $entities = [];

    // Loop trough all the <a> elements that don't have the LinkIt attributes.
    $xpath_query = "//a[@href != '']";
    foreach ($xpath->query($xpath_query) as $element) {
      /** @var \DOMElement $element */
      try {
        // Get the href value of the <a> element.
        $href = $element->getAttribute('href');
        $entity = $this->urlToEntity->findEntityIdByUrl($href);
        if ($entity) {
          $entity = $this->entityTypeManager->getStorage($entity['type'])->load($entity['id']);
          if ($element->hasAttribute('data-entity-uuid')) {
            // Normally the Linkit plugin handles when a element has this
            // attribute, but sometimes users may change the HREF manually and
            // leave behind the wrong UUID.
            $data_uuid = $element->getAttribute('data-entity-uuid');
            // If the UUID is the same as found in HREF, then skip it because
            // it's LinkIt's job to register this usage.
            if ($data_uuid === $entity->uuid()) {
              continue;
            }
          }
          yield [$entity, $element];
        }
      }
      catch (\DOMException) {
        // Do nothing.
      }
    }

    return $entities;
  }

  /**
   * Returns the language from an anchor's href.
   *
   * @param string $href
   *   The anchor's href attribute.
   *
   * @return string
   *   The language code from the href; or the site default if one couldn't be
   *   identified from the href, or the language module isn't enabled.
   */
  protected function getLanguage(string $href): ?string {
    if (!$this->languageNegotiator) {
      return Language::LANGCODE_SITE_DEFAULT;
    }
    $request = Request::create($href);
    $url_negotiation = $this->languageNegotiator->getNegotiationMethodInstance('language-url');
    return $url_negotiation->getLangcode($request) ?: Language::LANGCODE_SITE_DEFAULT;
  }

  /**
   * Returns the UUID of the given entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param $entity_id
   *   The entity ID.
   *
   * @return string
   *   The entity's UUID.
   */
  protected function getUuid(string $entity_type_id, $entity_id): string {
    return $this->entityTypeManager->getStorage($entity_type_id)
      ->load($entity_id)
      ->uuid();
  }

  /**
   * Checks if it's possible to create canonical links for a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return bool
   *   TRUE if the entity type supports canonical links.
   */
  protected function canCreateEntityLinks(string $entity_type_id): bool {
    return $this->entityTypeManager->getDefinition($entity_type_id)->hasLinkTemplate('canonical');
  }

}
