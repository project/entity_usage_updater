<?php

namespace Drupal\entity_usage_updater;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\entity_usage\UrlToEntityInterface;

trait LinkRemoverTrait {

  /**
   * Checks if two entity objects refer to the same entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $a
   *   The first entity to check.
   * @param \Drupal\Core\Entity\EntityInterface $b
   *   The second entity to check.
   *
   * @return bool
   *   TRUE if the objects refer to the same entity; FALSE otherwise.
   */
  abstract protected function sameEntity(EntityInterface $a, EntityInterface $b): bool;

  /**
   * Removes links to entities within a string property.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The target entity to remove references to.
   * @param \Drupal\Core\TypedData\Type\StringInterface $text
   *   The string item to search within.
   * @param \Drupal\entity_usage\UrlToEntityInterface $url_to_entity
   *   A UrlToEntity service object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  protected function removeLinks(EntityInterface $entity, StringInterface $text, UrlToEntityInterface $url_to_entity, EntityTypeManagerInterface $entity_type_manager): void {
    if (!trim($text->getValue())) {
      return;
    }

    $dom = Html::load($text->getValue());
    $elements_to_remove = [];

    foreach ($dom->getElementsByTagName('a') as $element) {
      /** @var \DOMElement $element */
      try {
        // Get the href value of the <a> element.
        $href = $element->getAttribute('href');
        $url_entity = $url_to_entity->findEntityIdByUrl($href);
        if ($url_entity === NULL) {
          continue;
        }
        $url_entity = $entity_type_manager->getStorage($url_entity['type'])->load($url_entity['id']);

        // If using LinkIt and the uuids match remove the link. Sometimes
        // users hack the url causing the data-entity-uuid not match. If
        // either the entities or the uuids match we should remove the link.
        if (
          ($url_entity && $this->sameEntity($entity, $url_entity)) ||
          ($element->hasAttribute('data-entity-uuid') && $element->getAttribute('data-entity-uuid') === $entity->uuid())
        ) {
          // Copy inner HTML to a new fragment.
          $fragment = $dom->createDocumentFragment();
          $fragment->appendXML(
            array_reduce(iterator_to_array($element->childNodes), fn(string $carry, \DomNode $child) => $carry . $dom->saveXML($child), '')
          );
          // Replace link with fragment.
          $element->parentNode->insertBefore($fragment, $element);
          // Remove links after discovery.
          $elements_to_remove[] = $element;
        }
      }
      catch (\DOMException) {
        // Do nothing.
      }
    }

    if (!empty($elements_to_remove)) {
      // Remove all the links.
      foreach ($elements_to_remove as $element) {
        $element->parentNode->removeChild($element);
      }
      $text->setValue(Html::serialize($dom));
    }
  }

}
