<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\PermissionCheckerInterface;
use Drupal\entity_usage_updater\EntityUsageUpdaterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for updating entity references.
 */
class EntityUsageUpdateForm extends FormBase {

  /**
   * The operation to remove links to an entity.
   */
  private const REMOVE = 'remove';

  /**
   * The operation to update links to an entity.
   */
  private const UPDATE = 'update';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity usage updater.
   *
   * @var \Drupal\entity_usage_updater\EntityUsageUpdaterInterface
   */
  protected EntityUsageUpdaterInterface $entityUsageUpdater;

  /**
   * The permission checker.
   *
   * @var \Drupal\Core\Session\PermissionCheckerInterface
   */
  protected PermissionCheckerInterface $permissionChecker;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $form = parent::create($container);
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->entityUsageUpdater = $container->get('entity_usage_updater.updater');
    $form->permissionChecker = $container->get('permission_checker');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_usage_updater_entity_usage_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?EntityInterface $entity = NULL) {
    // Deny access to the form if the user cannot edit or delete the entity (if
    // available) or they do not have the generic permission provided by the
    // module.
    $form['#access'] = $this->permissionChecker->hasPermission('update referenced entities', $this->currentUser()) ||
      ($entity && ($entity->access('update') || $entity->access('delete')));

    if ($form_state->has('operation')) {
      return $this->confirmForm($form, $form_state);
    }

    $is_content_entity_type = fn(EntityTypeInterface $type): bool => $type instanceof ContentEntityTypeInterface;
    $types = array_filter($this->entityTypeManager->getDefinitions(), $is_content_entity_type);
    $options = [];
    foreach ($types as $type) {
      $options[$type->id()] = $type->getLabel();
    }
    asort($options, SORT_LOCALE_STRING);

    if ($entity) {
      $form['entity_type_id'] = [
        '#type' => 'value',
        '#value' => $entity->getEntityTypeId(),
      ];
      $form['old_entity_id'] = [
        '#type' => 'value',
        '#value' => $entity->id(),
      ];
    }
    else {
      $form['entity_type_id'] = [
        '#title' => $this->t("Entity type"),
        '#type' => 'select',
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => isset($options['node']) ? 'node' : NULL,
      ];

      $form['old_entity_id'] = [
        '#title' => $this->t("Target entity ID"),
        '#description' => $this->t("Enter the ID of the entity you no longer want referenced."),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#size' => 20,
      ];
    }

    $form['new_entity_id'] = [
      '#title' => $this->t("Replacement entity ID"),
      '#description' => $this->t("Enter the ID of the entity you want to use instead."),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 20,
    ];

    $form['buttons'] = [
      '#type' => 'actions',
    ];
    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t("Update"),
      '#submit' => [[$this, 'updateUsages']],
      '#validate' => ['::updateUsageValidate'],
    ];
    $form['buttons']['remove'] = [
      '#type' => 'submit',
      '#value' => $this->t("Remove"),
      '#submit' => [[$this, 'removeUsages']],
      '#limit_validation_errors' => [['entity_type_id'], ['old_entity_id']],
    ];

    return $form;
  }

  /**
   * Validates a submission when update is pressed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function updateUsageValidate(array &$form, FormStateInterface $form_state): void {
    $ids = [
      $form_state->getValue('old_entity_id'),
      $form_state->getValue('new_entity_id'),
    ];
    if ($ids[0] === $ids[1]) {
      $form_state->setError($form, $this->t("The target and replacement IDs must be different."));
    }
    $entity_type_id = $form_state->getValue('entity_type_id');
    $type = $this->entityTypeManager->getDefinition($entity_type_id);
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery();
    $results = $query->accessCheck(FALSE)
      ->condition($type->getKey('id'), $ids, 'IN')
      ->execute();
    if (count($results) !== 2) {
      $context = ['@type' => $type->getLabel()];
      foreach (['old_entity_id', 'new_entity_id'] as $name) {
        $id = $form_state->getValue($name);
        if (!in_array($id, $results, TRUE)) {
          $context['@id'] = $id;
          $form_state->setErrorByName($name, $this->t("@type with ID @id doesn't exist.", $context));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $operation = $form_state->get('operation');
    if (!method_exists($this->entityUsageUpdater, $operation)) {
      throw new \LogicException('Illegal operation: ' . $operation);
    }
    $this->entityUsageUpdater->$operation($form_state->get('argument'));
  }

  /**
   * Processes a submission when remove is pressed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function removeUsages(array &$form, FormStateInterface $form_state): void {
    $entity_type_id = $form_state->getValue('entity_type_id');
    $old_entity_id = $form_state->getValue('old_entity_id');

    // Argument for \Drupal\entity_usage_updater\EntityUsageUpdater::remove().
    $argument = [
      $entity_type_id => [
        $old_entity_id,
      ],
    ];

    $form_state
      ->set('operation', self::REMOVE)
      ->set('argument', $argument)
      ->set('description', $this->t('Are you sure you want to remove links to %old_entity?', [
        '%old_entity' => $this->getEntityLabel($entity_type_id, $old_entity_id),
      ]))
      ->setRebuild();

  }

  /**
   * Processes a submission when update is pressed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function updateUsages(array &$form, FormStateInterface $form_state): void {
    $entity_type_id = $form_state->getValue('entity_type_id');
    $old_entity_id = $form_state->getValue('old_entity_id');
    $new_entity_id = $form_state->getValue('new_entity_id');

    // Argument for \Drupal\entity_usage_updater\EntityUsageUpdater::update().
    $argument = [
      $entity_type_id => [
        $old_entity_id => [
          $entity_type_id,
          $new_entity_id,
        ],
      ],
    ];

    $form_state
      ->set('operation', self::UPDATE)
      ->set('argument', $argument)
      ->set('description', $this->t('Are you sure you want to replace links to %old_entity with links to %new_entity?', [
        '%old_entity' => $this->getEntityLabel($entity_type_id, $old_entity_id),
        '%new_entity' => $this->getEntityLabel($entity_type_id, $new_entity_id),
      ]))
      ->setRebuild();
  }

  /**
   * Gets the label for an entity or the url if no label exists.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string|int $id
   *   The entity id.
   *
   * @return string|\Stringable
   *   The label for an entity or the url if no label exists.
   */
  private function getEntityLabel(string $entity_type, string|int $id): string|\Stringable {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($id);
    return $entity->label() ?: $entity->toUrl()->toString();
  }

  /**
   * Creates the confirm form from form state.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   The confirm form.
   */
  protected function confirmForm(array &$form, FormStateInterface $form_state): array {
    $form['#attributes']['class'][] = 'confirmation';
    $form['description'] = ['#markup' => $form_state->get('description')];
    $form['buttons'] = [
      '#type' => 'actions',
    ];

    $form['buttons']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];

    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * Provides custom submission handler for 'Cancel' button (confirm form).
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state): void {
    $storage = &$form_state->getStorage();
    NestedArray::unsetValue($storage, ['operation']);
    $form_state->setRebuild();
  }

}
