<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModeration;
use Drupal\content_moderation\StateTransitionValidationInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_usage\UrlToEntityInterface;
use Drupal\entity_usage_updater\EntityUsageUpdater;
use Drupal\entity_usage_updater\EntityUsageUpdaterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for updating entity references.
 */
class LinkRemoverForm extends FormBase {

  /**
   * The entity usage updater.
   *
   * @var \Drupal\entity_usage_updater\EntityUsageUpdaterInterface
   */
  protected EntityUsageUpdaterInterface $entityUsageUpdater;

  /**
   * The URL helper.
   *
   * @var \Drupal\entity_usage\UrlToEntityInterface
   */
  protected UrlToEntityInterface $urlToEntity;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The content moderation information service.
   *
   * This service will only be available if content_moderation is installed.
   */
  protected ?ModerationInformationInterface $moderationInfo = NULL;

  /**
   * The state transition validation service.
   *
   * This service will only be available if content_moderation is installed.
   */
  protected ?StateTransitionValidationInterface $contentModerationValidation = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $form = parent::create($container);
    $form->entityUsageUpdater = $container->get('entity_usage_updater.updater');
    $form->urlToEntity = $container->get(UrlToEntityInterface::class);
    $form->entityTypeManager = $container->get('entity_type.manager');
    if ($container->has('content_moderation.state_transition_validation')) {
      $form->contentModerationValidation = $container->get('content_moderation.state_transition_validation');
    }
    if ($container->has('content_moderation.moderation_information')) {
      $form->moderationInfo = $container->get('content_moderation.moderation_information');
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_usage_updater_link_remover';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($form_state->has('argument')) {
      return $this->confirmForm($form, $form_state);
    }

    $form['links'] = [
      '#title' => $this->t("Links to remove"),
      '#description' => $this->t("Enter the links you want to remove from this site, 1 per line."),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#rows' => 10,
    ];

    $form['unpublish'] = [
      '#title' => $this->t("Unpublish content"),
      '#type' => 'checkbox',
    ];

    $form['buttons'] = [
      '#type' => 'actions',
    ];

    $form['buttons']['remove'] = [
      '#type' => 'submit',
      '#value' => $this->t("Remove"),
      '#submit' => [[$this, 'removeUsages']],
      '#validate' => ['::validateList'],
    ];

    return $form;
  }

  /**
   * Validates a submission when update is pressed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateList(array &$form, FormStateInterface $form_state): void {
    $argument = $urls_not_found = [];

    $list = array_map('trim', explode("\n", $form_state->getValue('links')));
    foreach ($list as $url) {
      // Ignore empty lines.
      if (empty($url)) {
        continue;
      }
      $entity = $this->urlToEntity->findEntityIdByUrl($url);
      if (!$entity) {
        $urls_not_found[] = $url;
        continue;
      }
      if (!isset($argument[$entity['type']])) {
        $argument[$entity['type']] = [];
      }
      $argument[$entity['type']][] = $entity['id'];
    }

    if (!empty($urls_not_found)) {
      // @todo format the list better.
      $form_state->setErrorByName('links', $this->t('The following URLs can not be matched: @urls', ['@urls' => implode(', ', $urls_not_found)]));
    }
    else {
      $form_state->set('argument', $argument);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $moderation = [];
    $moderation_table_values = $form_state->getValue('table');
    // It is possible there are no moderation forms added.
    if (is_array($moderation_table_values)) {
      foreach ($moderation_table_values as $key => $info) {
        [$entity_type_id, $id] = explode('|', $key, 2);
        if (isset($info['moderation']) && $info['moderation'] !== '') {
          $moderation[$entity_type_id][$id] = $info['moderation'];
        }
      }
    }
    $this->entityUsageUpdater->remove($form_state->get('argument'), $moderation);
  }

  /**
   * Processes a submission when remove is pressed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function removeUsages(array &$form, FormStateInterface $form_state): void {
    $form_state->set('unpublish', (bool) $form_state->getValue('unpublish'));
    $form_state->setRebuild();
  }

  /**
   * Creates the confirm form from form state.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   The confirm form.
   */
  protected function confirmForm(array &$form, FormStateInterface $form_state): array {
    $form['#attributes']['class'][] = 'confirmation';

    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Title'),
        $this->t('Entity type'),
        $this->t('Active usage count'),
        $this->t('Moderation state'),
      ],
      '#tree' => TRUE,
    ];

    if ($form_state->get('unpublish')) {
      $form['table']['#header'][] = $this->t('Update moderation state to');
    }

    foreach ($this->getEntitiesFromArgument($form_state) as $entity) {
      $usage_count = array_reduce(EntityUsageUpdater::getUsages($entity), function ($result, $item) {
        return $result + count($item);
      }, 0);

      $key = $entity->getEntityTypeId() . '|' . $entity->id();
      $form['table'][$key]['label'] = [
        '#type' => 'link',
        '#title' => $entity->label() ?: $entity->id(),
        '#url' => $entity->toUrl(),
      ];
      $form['table'][$key]['type'] = [
        '#markup' => $entity->getEntityType()->getLabel(),
      ];
      $form['table'][$key]['usage_count'] = [
        '#markup' => $usage_count,
      ];
      $this->addContentModerationInfo($form['table'][$key], $entity, $form_state->get('unpublish'));
    }

    $form['description'] = ['#markup' => $this->t('Are you sure you want to remove links to the listed entities?')];
    $form['buttons'] = [
      '#type' => 'actions',
    ];

    $form['buttons']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancelForm'],
      '#limit_validation_errors' => [],
    ];

    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * Gets the entities listed by the user once the list has been validated.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The loaded entities that will be updated.
   */
  protected function getEntitiesFromArgument(FormStateInterface $form_state): array {
    $entities = [];
    $argument = $form_state->get('argument') ?? [];
    foreach ($argument as $entity_type => $entity_ids) {
      $entities = array_merge($entities, array_values($this->entityTypeManager->getStorage($entity_type)->loadMultiple($entity_ids)));
    }
    return $entities;
  }

  /**
   * Provides custom submission handler for 'Cancel' button (confirm form).
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state): void {
    $storage = &$form_state->getStorage();
    NestedArray::unsetValue($storage, ['argument']);
    NestedArray::unsetValue($storage, ['unpublish']);
    $form_state->setRebuild();
  }

  /**
   * Adds info about an entity's moderation state to a row in the form.
   *
   * @param array $row
   *   The row to add moderation information to.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to add the moderation form for.
   * @param bool $add_form
   *   Determines whether to return a form, if necessary to change the state.
   */
  public function addContentModerationInfo(array &$row, EntityInterface $entity, bool $add_form): void {
    if ($this->moderationInfo === NULL || !$this->moderationInfo->isModeratedEntity($entity)) {
      if (!$entity instanceof EntityPublishedInterface) {
        return;
      }
      $target_states = [
        'unpublished' => $this->t('Unpublished'),
        'published' => $this->t('Published'),
      ];
      $current_state = $entity->isPublished() ? 'published' : 'unpublished';
      $row['current_moderation_state'] = [
        '#markup' => $target_states[$current_state],
      ];
      if (!$add_form) {
        return;
      }

      $default_value = 'unpublished';
    }
    else {
      assert($entity instanceof ContentEntityInterface);
      $default_value = $current_state = $entity->moderation_state->value;

      // Get the label for the current state.
      $workflow = $this->moderationInfo->getWorkflowForEntity($entity);
      $row['current_moderation_state'] = [
        '#markup' => $workflow->getTypePlugin()->getState($current_state)->label(),
      ];
      if (!$add_form) {
        return;
      }

      /** @var \Drupal\workflows\Transition[] $transitions */
      $transitions = $this->contentModerationValidation->getValidTransitions($entity, $this->currentUser());
      $target_states = [];

      foreach ($transitions as $transition) {
        $target_states[$transition->to()->id()] = $transition->to()->label();
        $to = $transition->to();
        // Remove the transition if we know it is to a published state.
        if ($to instanceof ContentModerationState && $to->isPublishedState()) {
          unset($target_states[$transition->to()->id()]);
        }
      }

      $default_content_moderation_unpublish_state = $workflow->getThirdPartySetting('entity_usage_updater', 'default_unpublished_state');
      if ($default_content_moderation_unpublish_state !== NULL && isset($target_states[$default_content_moderation_unpublish_state])) {
        $default_value = $default_content_moderation_unpublish_state;
      }
      // Fallback to transition to a state that is unpublished and the default
      // revision.
      elseif ($workflow->getTypePlugin() instanceof ContentModeration) {
        foreach ($transitions as $transition) {
          /** @var \Drupal\content_moderation\ContentModerationState $to */
          $to = $transition->to();
          if ($to->isDefaultRevisionState() && !$to->isPublishedState()) {
            $default_value = $to->id();
          }
        }
      }
    }

    if (!$entity->access('update')) {
      $row['moderation'] = [
        '#markup' => $this->t('You do not have permission to change.'),
      ];
      return;
    }

    // Remove the current state as there is no point changing to it.
    unset($target_states[$current_state]);

    // If there are no target states then there is nothing for the user to
    // select.
    if (count($target_states) < 1) {
      return;
    }

    $row['moderation'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('- Select to change -'),
      '#title' => $this->t('Update moderation state to'),
      '#title_display' => 'invisible',
      '#default_value' => isset($target_states[$default_value]) ? $default_value : NULL,
      '#options' => $target_states,
    ];
  }

}
