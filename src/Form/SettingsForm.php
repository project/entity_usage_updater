<?php

declare(strict_types = 1);

namespace Drupal\entity_usage_updater\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Render\Element;
use Drupal\workflows\Entity\Workflow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure Entity Usage Updater.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The entity usage updater plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * Flag indicating if content moderation is enabled.
   */
  protected bool $contentModerationEnabled;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->pluginManager = $container->get('plugin.manager.entity_usage_updater');
    $form->contentModerationEnabled = $container->get('module_handler')->moduleExists('content_moderation');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_usage_updater_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['entity_usage_updater.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['#tree'] = TRUE;
    foreach ($this->pluginManager->getDefinitions() as $definition) {
      if (is_a($definition['class'], PluginFormInterface::class, TRUE)) {
        $id = $definition['id'];
        $instance = $this->createInstance($id);
        $form['plugin'][$id] = $instance->buildConfigurationForm([], $form_state);
      }
    }

    if ($this->contentModerationEnabled) {
      foreach (Workflow::loadMultipleByType('content_moderation') as $workflow) {
        $options = [];
        foreach ($workflow->getTypePlugin()->getStates() as $state) {
          /** @var \Drupal\content_moderation\ContentModerationState $state */
          if (!$state->isPublishedState() && $state->isDefaultRevisionState()) {
            $options[$state->id()] = $state->label();
          }
        }
        if (!empty($options)) {
          $form['workflow'][$workflow->id()] = [
            '#type' => 'select',
            '#options' => $options,
            '#title' => $this->t('Select default unpublished state for %label workflow', ['%label' => $workflow->label()]),
          ];
        }
      }

      if (isset($form['workflow'])) {
        $form['workflow']['#type'] = 'fieldset';
        $form['workflow']['#title'] = $this->t('Workflow settings');
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    foreach (Element::children($form['plugin']) as $id) {
      $plugin_form = $form['plugin'][$id];
      $instance = $this->createInstance($id);
      $subform_state = SubformState::createForSubform($plugin_form, $form, $form_state);
      $instance->validateConfigurationForm($plugin_form, $subform_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    foreach (Element::children($form['plugin']) as $id) {
      $plugin_form = $form['plugin'][$id];
      $instance = $this->createInstance($id);
      $subform_state = SubformState::createForSubform($plugin_form, $form, $form_state);
      $instance->submitConfigurationForm($plugin_form, $subform_state);
    }

    $config = $this->config('entity_usage_updater.settings');
    foreach ($form_state->getValue('plugin') as $id => $configuration) {
      $config->set("plugins.$id", $configuration);
    }
    $config->save();

    if ($this->contentModerationEnabled) {
      $form_workflow_value = $form_state->getValue('workflow') ?? [];
      foreach (Workflow::loadMultipleByType('content_moderation') as $workflow) {
        if (isset($form_workflow_value[$workflow->id()])) {
          $workflow
            ->setThirdPartySetting('entity_usage_updater', 'default_unpublished_state', $form_workflow_value[$workflow->id()])
            ->save();
        }
        elseif ($workflow->getThirdPartySetting('entity_usage_updater', 'default_unpublished_state') !== NULL) {
          $workflow->unsetThirdPartySetting('entity_usage_updater', 'default_unpublished_state')->save();
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Creates an instance of the plugin with the stored configuration.
   *
   * @param string $id
   *   The plugin ID.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The plugin.
   */
  protected function createInstance(string $id): PluginFormInterface {
    $config = $this->config('entity_usage_updater.settings');
    $configuration = $config->get('plugins.' . $id) ?? [];
    return $this->pluginManager->createInstance($id, $configuration);
  }

}
