<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater;

/**
 * Supports updating references to entities.
 *
 * It will only update references tracked by the Entity Usage module.
 */
interface EntityUsageUpdaterInterface {

  /**
   * Updates references to entities via the batch API.
   *
   * @param array $mapping
   *   An associative array specifying which entity references to replace. For
   *   example, to update references to node 7946 to 6906, and term 4056 to
   *   4051, pass:
   *   @code
   *   [
   *     'node' => [
   *       7946 => ['node', 6906],
   *     ],
   *     'taxonomy_term' => [
   *       4056 => ['taxonomy_term', 4051],
   *     ],
   *   ];
   *   @endcode
   *   Note that using a different target entity type could lead to invalid
   *   state or errors.
   */
  public function update(array $mapping): void;

  /**
   * Removes references to entities via the batch API.
   *
   * @param array $mapping
   *   An associative array specifying which entity references to remove. For
   *   example, to remove references to node 7946 and term 4056, pass:
   *   @code
   *   [
   *     'node' => [
   *       7946,
   *     ],
   *     'taxonomy_term' => [
   *       4056,
   *     ],
   *   ];
   *   @endcode
   * @param array $moderation
   *   An associative array specifying a state to set the entity in after
   *   removing references. This array supports content moderation states. For
   *   example, to change the state to archived for node 7948, pass:
   *   @code
   *   [
   *     'node' => [
   *       7946 => 'archived',
   *     ],
   *   ]
   *   @endcode
   *   Note this only support entities that have been listed in $mapping.
   */
  public function remove(array $mapping, array $moderation = []): void;

}
