<?php

declare(strict_types=1);

namespace Drupal\entity_usage_updater\Utility;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Helper to convert violations into a message.
 */
final class ViolationsHelper {

  /**
   * Prevents construction.
   */
  private function __construct() {}

  /**
   * Converts EntityConstraintViolationListInterface to a log message.
   *
   * @param \Drupal\Core\Entity\EntityConstraintViolationListInterface $violations
   *   The list of violations generated during the entity validation.
   *
   * @return array{message:string, context:array<string, string>}
   *   An array containing arguments to pass to a logger.
   */
  public static function toLogMessage(EntityConstraintViolationListInterface $violations): array {
    $entity = $violations->getEntity();

    $log_message = 'Cannot save %entity_type %entity_id';
    $context = [
      '%entity_type' => $entity->getEntityTypeId(),
      '%entity_id' => $entity->id(),
    ];

    if ($entity instanceof RevisionableInterface && $revision_id = $entity->getRevisionId()) {
      $log_message .= ' (Revision ID: %revision_id)';
      $context['%revision_id'] = $revision_id;
    }

    $violation_messages = [];
    foreach ($violations as $violation) {
      $violation_messages[] = sprintf('%s=%s', $violation->getPropertyPath(), $violation->getMessage());
    }
    $context['@violations'] = implode(', ', $violation_messages);
    $log_message .= ' due to: @violations';

    return ['message' => $log_message, 'context' => $context];
  }

  /**
   * Converts EntityConstraintViolationListInterface to translatable markup.
   *
   * @param \Drupal\Core\Entity\EntityConstraintViolationListInterface $violations
   *   The list of violations generated during the entity validation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The message to display to a user.
   */
  public static function toTranslatableMarkup(EntityConstraintViolationListInterface $violations): TranslatableMarkup {
    $entity = $violations->getEntity();

    $messages = [];
    foreach ($violations as $violation) {
      $messages[] = (string) $violation->getMessage();
    }
    $violation_message = Markup::create(implode(', ', $messages));

    if ($entity instanceof RevisionableInterface && $revision_id = $entity->getRevisionId()) {
      return new TranslatableMarkup(
        'Cannot save %entity_type %entity_id (revision: %revision_id) because: @violations',
        [
          '%entity_type' => $entity->getEntityTypeId(),
          '%entity_id' => $entity->id(),
          '%revision_id' => $revision_id,
          '@violations' => $violation_message,
        ],
      );
    }
    return new TranslatableMarkup(
      'Cannot save %entity_type %entity_id because: @violations',
      [
        '%entity_type' => $entity->getEntityTypeId(),
        '%entity_id' => $entity->id(),
        '@violations' => $violation_message,
      ],
    );
  }

}
